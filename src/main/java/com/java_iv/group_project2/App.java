package com.java_iv.group_project2;

import com.java_iv.group_project2.controller.Controller;
import com.java_iv.group_project2.controller.PaymentStrategy.Payment;
import com.java_iv.group_project2.model.EWallet;
import com.java_iv.group_project2.view.*;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        //pane.setPadding(new Insets(11.5, 12.5, 13.5, 14.5));
        pane.setHgap(25);
        pane.setVgap(5.5);

        Payment payment = new Payment();
        Controller controller = new Controller();
        MainView view = new MainView(controller);
        EWallet model = new EWallet();
        controller.setViewModel(view, model, payment);

        pane.add(view, 0, 0);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add("style.css");

        stage.setScene(scene);
        stage.setTitle("E-wallet");
        stage.getIcons().add(new Image("file:src/main/java/com/java_iv/group_project2/images/cash.png"));

        stage.setOnShowing(e -> {
            view.doGreeting();
        });
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}