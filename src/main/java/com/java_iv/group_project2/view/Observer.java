package com.java_iv.group_project2.view;

public interface Observer {
    public void update(double amount);
}
