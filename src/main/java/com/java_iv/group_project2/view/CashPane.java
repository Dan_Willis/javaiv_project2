package com.java_iv.group_project2.view;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

public class CashPane extends VBox implements Observer{
    private HBox cashBox;
    private Button pay;
    private Button add;
    private PaymentField paymentField;
    private TextField cashAmount;
    private Text message;
    

    public CashPane() {
        this.getStyleClass().add("cash-pane");
        this.setSpacing(5);
        this.setPadding(new Insets(10, 0, 0, 0));
        this.cashAmount = new TextField("Cash: $0.00");
        this.cashAmount.setEditable(false);
        this.cashAmount.setMouseTransparent(true);
        this.cashAmount.setFocusTraversable(false);
        this.cashAmount.setStyle("-fx-font-weight: 700;" +
                "-fx-background-color: #0a192f;" +
                "-fx-font-size: 15;" +
                "-fx-text-fill: #56D3BF;");

        this.cashBox = new HBox(5);
        this.pay = new Button("Pay");
        this.add = new Button("Add");
        this.message = new Text();
        this.message.setFill(Paint.valueOf("#CED9F9FF"));

        this.paymentField = new PaymentField();
        HBox.setHgrow(this.paymentField, Priority.ALWAYS);

        this.cashBox.getChildren().addAll(this.pay, this.add, this.paymentField);
        this.getChildren().addAll(this.cashAmount, this.cashBox, this.message);
        this.setMaxWidth(300);
    }

    /**
     * Setter and getters
     */
    public Button getPay() {
        return pay;
    }

    public Button getAdd() {
        return add;
    }

    public PaymentField getPaymentField() {
        return paymentField;
    }

    public void setMessage(String message) {
        this.message.setText(message);
    }

    /**
     * Update method for the observer that updates the cash value in the UI
     */
    @Override
    public void update(double amount) {
        this.cashAmount.setText("Cash: $" + String.format("%,.2f", amount));
    }
}
