package com.java_iv.group_project2.view;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

public abstract class WalletItemUI extends StackPane {
    private Rectangle item;
    public WalletItemUI() {
        this.getStyleClass().add("wallet-item-box");
        item = new Rectangle();
        this.item.getStyleClass().add("wallet-item");
        this.item.setX(50);
        this.item.setY(50);
        this.item.setWidth(200);
        this.item.setHeight(100);
        this.item.setArcWidth(20);
        this.item.setArcHeight(20);
        this.item.setFill(Paint.valueOf("#CED9F9FF"));

        this.getChildren().add(this.item);
    }

    /**
     * changes the color of the card
     * @param isSelected
     */
    public void makeSelected(boolean isSelected){
        if(isSelected){
            if (this instanceof CardUI) {
                this.item.setFill(Paint.valueOf("#1D3181BC"));
                this.setOnMouseExited(e -> {
                    this.item.setFill(Paint.valueOf("#CED9F9FF"));
                });
            } else {
                this.item.setFill(Paint.valueOf("#ff66cc"));
                this.setOnMouseExited(e -> {
                    this.item.setFill(Paint.valueOf("#ff66cc"));
                });
            }
        } else {
            this.item.setFill(Paint.valueOf("#CED9F9FF"));
            this.setOnMouseExited(e -> {
                this.item.setFill(Paint.valueOf("#CED9F9FF"));
            });
        }
    }


    /**
     * Getters and setters
     */
    public Rectangle getItem() {
        return item;
    }

    public abstract int getItemId();

    public abstract void updateText(String newText);
}
