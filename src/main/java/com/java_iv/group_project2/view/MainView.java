package com.java_iv.group_project2.view;

import java.io.File;

import com.java_iv.group_project2.controller.Controller;
import com.java_iv.group_project2.view.notesView.NoteView;

import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.util.Duration;

/**
 * represents the hole view
 */
public class MainView extends AnchorPane {
    private Controller controller;

    private CardView cardView;
    private NoteView noteView;
    private Button saveBtn;
    private Button loadBtn;
    private TabPane walletTabs;
    private Tab cards;
    private Tab notes;

    private Text notificationText;
    private boolean isNotifAnimPlaying;

    private Text messageText;
    private ImageView favoritePic;
    private Button btnChoosePic;
    private HBox imageForm;
    private PathTransition waitingAnim;
    private PathTransition greetingAnim;
    private ImageView greetingPic;

    private HBox topBar;

    public MainView(Controller controller) {
        this.getStyleClass().add("main-view");
        this.notificationText = new Text("(Reminders here)");
        this.notificationText.getStyleClass().add("notification");
        this.notificationText.textProperty().addListener(o -> this.playAnim(this.notificationText));
        this.controller = controller;
        this.cardView = new CardView();
        this.noteView = new NoteView(controller, this.notificationText);
        this.walletTabs = new TabPane();
        this.saveBtn = new Button("Save E-wallet");
        this.loadBtn = new Button("Load E-wallet");
        this.messageText = new Text();
        this.messageText.getStyleClass().add("message-text");

        this.greetingPic = new ImageView(new Image("file:src/main/java/com/java_iv/group_project2/images/hands.png"));
        this.favoritePic = new ImageView(new Image("file:src/main/java/com/java_iv/group_project2/images/cash.png"));
        this.btnChoosePic = new Button("Choose Pic:");
        this.btnChoosePic.setOnAction(e -> chooseFavoritePic());
        this.imageForm = new HBox(btnChoosePic, this.favoritePic);
        this.favoritePic.setFitHeight(50);
        this.favoritePic.setFitWidth(150);
        this.favoritePic.setPreserveRatio(true);
        this.imageForm.setSpacing(20);

        this.topBar = new HBox(this.messageText, this.notificationText, this.saveBtn, this.loadBtn, this.imageForm);
        this.topBar.setSpacing(10);

        AnchorPane.setTopAnchor(this.walletTabs, 5.0);
        AnchorPane.setLeftAnchor(this.walletTabs, 0.0);
        AnchorPane.setRightAnchor(this.walletTabs, 0.0);
        AnchorPane.setTopAnchor(this.topBar, 15.0);
        AnchorPane.setRightAnchor(this.topBar, 15.0);
        this.walletTabs.setStyle("-fx-padding: 2 0 0 0;");

        this.cards = new Tab("Cards");
        this.notes = new Tab("Notes");

        this.cards.setStyle("-fx-pref-height: 50;" +
                "-fx-pref-width: 110;");
        this.notes.setStyle("-fx-pref-height: 50;" +
                "-fx-pref-width: 110;");

        this.cards.setContent(this.cardView);
        this.notes.setContent(this.noteView);

        this.walletTabs.getTabs().addAll(this.cards, this.notes);
        this.walletTabs.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        this.getChildren().addAll(walletTabs, this.topBar);

        this.saveBtn.setOnAction(e -> {this.controller.saveEWalletToDB();});
        this.loadBtn.setOnAction(e -> {this.controller.loadEWalletFromDB();});
        this.messageText.textProperty().addListener(o -> playAnim(this.messageText));

    }

    public Text getMessageText() {
        return messageText;
    }

    /**
     * takes care of letting the user choose their favorite pic
     */
    private void chooseFavoritePic(){
        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        FileChooser.ExtensionFilter extFilterJPG
                = new FileChooser.ExtensionFilter("JPG files (*.JPG)", "*.JPG");
        FileChooser.ExtensionFilter extFilterjpg
                = new FileChooser.ExtensionFilter("jpg files (*.jpg)", "*.jpg");
        FileChooser.ExtensionFilter extFilterPNG
                = new FileChooser.ExtensionFilter("PNG files (*.PNG)", "*.PNG");
        FileChooser.ExtensionFilter extFilterpng
                = new FileChooser.ExtensionFilter("png files (*.png)", "*.png");
        fileChooser.getExtensionFilters()
                .addAll(extFilterJPG, extFilterjpg, extFilterPNG, extFilterpng);
        //Show open file dialog
        
        File file = fileChooser.showOpenDialog(null);

        if(file != null){
            this.favoritePic.setImage(new Image(file.toURI().toString()));
        } 
        
        
    }

    /**
     * plays a waiting animation in the bottom right corner
     */
    public void playWaitingAnim() {

        Circle circle = new Circle(20);
        ImageView iView = new ImageView(this.favoritePic.getImage());
        iView.setFitHeight(50);
        iView.setFitWidth(150);
        iView.setPreserveRatio(true);

        AnchorPane.setRightAnchor(iView, 0.0);
        AnchorPane.setBottomAnchor(iView, 10.0);
        this.getChildren().add(iView);

        this.waitingAnim = new PathTransition();
        this.waitingAnim.setDuration(Duration.millis(1000));
        this.waitingAnim.setPath(circle);
        this.waitingAnim.setNode(iView);
        this.waitingAnim.setCycleCount(Timeline.INDEFINITE);
        this.waitingAnim.play();
    }


    /**
     * stops the waiting animation (loading and saving)
     */
    public void stopWaitingAnim(){
        this.waitingAnim.stop();
        this.getChildren().remove(this.waitingAnim.getNode());
        this.waitingAnim = null;
    }

    /**
     * plays the greeting animation
     */
    public void playGreetingAnim() {

        Circle circle = new Circle(20);
        ImageView iView = this.greetingPic;
        iView.setFitHeight(50);
        iView.setFitWidth(150);
        iView.setPreserveRatio(true);

        AnchorPane.setRightAnchor(iView, 0.0);
        AnchorPane.setBottomAnchor(iView, 10.0);
        this.getChildren().add(iView);

        this.greetingAnim = new PathTransition();
        this.greetingAnim.setDuration(Duration.millis(1000));
        this.greetingAnim.setPath(circle);
        this.greetingAnim.setNode(iView);
        this.greetingAnim.setCycleCount(Timeline.INDEFINITE);
        this.greetingAnim.play();
//        this.greetingAnim.setOnFinished(e -> {
//            this.stopGreetingAnim();
//        });


    }

    /**
     * stops the greeting animation
     */
    public void stopGreetingAnim(){
        this.greetingAnim.stop();
        this.getChildren().remove(this.greetingAnim.getNode());
        this.greetingAnim = null;
    }

    /**
     * does the greeting for the user at the start
     */
    public void doGreeting(){
        this.playGreetingAnim();
        messageText.setText("Greetings Ongoing: It's gonna take time");
        Thread t = new Thread(new Greeting(this.messageText, this));
        t.start();
    }


    /**
     * disables or enables the card and note view (not the topbar)
     */
    public void setViewsDisabled(boolean isDisabled){
        this.cardView.setDisable(isDisabled);
        this.noteView.setDisable(isDisabled);
    }

    /**
     * disables or enables the save and load, and choose pic buttons
     * @param isDisabled
     */
    public void setSaveLoadDisabled(boolean isDisabled){
        this.saveBtn.setDisable(isDisabled);
        this.loadBtn.setDisable(isDisabled);
        this.btnChoosePic.setDisable(isDisabled);
    }

    /**
     * plays a rotate animation
     *
     * @param messageText
     */
    private void playAnim(Text messageText) {
        if (!this.isNotifAnimPlaying) {
            this.isNotifAnimPlaying = true;
            RotateTransition rt = new RotateTransition(Duration.millis(100), messageText);
            rt.setByAngle(10);
            rt.setAutoReverse(true);
            rt.setCycleCount(4);
            rt.play();
            this.isNotifAnimPlaying = false;
        }

    }

    /**
     * Methods to set event listeners for pay, add, and remove buttons
     */

    // CashPane buttons
    public void setCashPanePayBtnListener(EventHandler<ActionEvent> event) {
        this.cardView.getCashPane().getPay().setOnAction(event);
    }

    public void setCashPaneAddBtnListener(EventHandler<ActionEvent> event) {
        this.cardView.getCashPane().getAdd().setOnAction(event);
    }

    // Info Pane buttons
    public void setInfoPaneDeleteBtnListener(EventHandler<ActionEvent> event) {
        this.cardView.getCardInfo().getDeleteBtn().setOnAction(event);
    }

    public void setInfoPanePayBtnListener(EventHandler<ActionEvent> event) {
        this.cardView.getCardInfo().getPayBtn().setOnAction(event);
    }

    // AddCardPane button
    public void setAddCardBtnListener(EventHandler<ActionEvent> event) {
        this.cardView.getAddCardPane().getAddButton().setOnAction(event);
    }

    // Deck buttons
    public void setDebitAddBtnListener(EventHandler<ActionEvent> event) {
        this.cardView.getCardPane().getDebitCardBtn().setOnAction(event);
    }

    public void setCreditAddBtnListener(EventHandler<ActionEvent> event) {
        this.cardView.getCardPane().getCreditCardBtn().setOnAction(event);
    }

    public void setPersonalAddBtnListener(EventHandler<ActionEvent> event) {
        this.cardView.getCardPane().getPersonalCardBtn().setOnAction(event);
    }

    /**
     * Remove a card from the deck based on card number
     */
    public void removeDebitCard(String cardNumber) {
        VBox deck = this.cardView.getCardPane().getDebitCards().getDeck();
        ObservableList<Node> cards = deck.getChildren();
        for (Node card : cards) {
            if (String.valueOf(card).equals(cardNumber)) {
                cards.remove(card);
                return;
            }
        }
    }

    /**
     * Getters
     */

    public CardView getCardView() {
        return cardView;
    }

    /**
     * Setters
     */

    public void setCardView(CardView cardView) {
        this.cardView = cardView;
    }

    public NoteView getNoteView() {
        return noteView;
    }

    /**
     * Show selected Card to infoPane
     */

    public void cardSelected(CardDeck cards, CardUI paymentCard) {
        cards.makeSelected(String.valueOf(paymentCard));
        CardUI currentCard = cards.getSelectedCard();
        this.getCardView().showCardDetails(currentCard);
    }

    public void paymentCardSelected(CardDeck cards, PaymentCardUI paymentCard) {
        cards.makeSelected(String.valueOf(paymentCard));
        CardUI currentCard = cards.getSelectedCard();
        String cardType = String.valueOf(currentCard.getParent().getParent().getParent().getParent().getParent());
        switch (cardType) {
            case "Debit Cards":
                this.getCardView().showPaymentCardDetails((PaymentCardUI) currentCard, "Funds: $");
                break;
            case "Credit Cards":
                this.getCardView().showPaymentCardDetails((PaymentCardUI) currentCard, "Limit: $");
                break;
        }
    }

    /**
     * Add a Card to the deck
     */

    // Debit
    public void addDebitCard(PaymentCardUI paymentCard) {
        paymentCard.setOnMousePressed(e -> {
            CardDeck cards = this.cardView.getCardPane().getDebitCards();
            paymentCardSelected(cards, paymentCard);
            this.cardView.getCardPane().setCurrentSelectedCard(paymentCard);
        });
        paymentCard.setOnMouseReleased(e -> paymentCard.getItem().setFill(Paint.valueOf("#CED9F9FF")));
        paymentCard.setOnMouseExited(e -> paymentCard.getItem().setFill(Paint.valueOf("#CED9F9FF")));
        this.cardView.getCardPane().getDebitCards().addWalletItem(paymentCard);
    }

    // Credit
    public void addCreditCard(PaymentCardUI paymentCard) {
        paymentCard.setOnMousePressed(e -> {
            CardDeck cards = this.cardView.getCardPane().getCreditCards();
            paymentCardSelected(cards, paymentCard);
            this.cardView.getCardPane().setCurrentSelectedCard(paymentCard);
        });
        paymentCard.setOnMouseReleased(e -> paymentCard.getItem().setFill(Paint.valueOf("#CED9F9FF")));
        paymentCard.setOnMouseExited(e -> paymentCard.getItem().setFill(Paint.valueOf("#CED9F9FF")));
        this.cardView.getCardPane().getCreditCards().addWalletItem(paymentCard);
    }

    // Personal
    public void addPersonalCard(CardUI card) {
        card.setOnMousePressed(e -> {
            CardDeck cards = this.cardView.getCardPane().getPersonalCards();
            cardSelected(cards, card);
            this.cardView.getCardPane().setCurrentSelectedCard(card);
        });
        card.setOnMouseReleased(e -> card.getItem().setFill(Paint.valueOf("#CED9F9FF")));
        card.setOnMouseExited(e -> card.getItem().setFill(Paint.valueOf("#CED9F9FF")));
        this.cardView.getCardPane().getPersonalCards().addWalletItem(card);
    }
}
