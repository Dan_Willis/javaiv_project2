package com.java_iv.group_project2.view;

import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

import java.util.regex.Pattern;

public class PaymentField extends TextField {
    private TextFormatter<String> formatter;
    private Pattern pattern;

    /**
     * Sets the text-field to only use a currency format
     */
    public PaymentField() {
        this.setPromptText("Enter cash amount");
        this.pattern = Pattern.compile("^\\d*|^\\d+\\.\\d{0,2}");
        this.formatter = new TextFormatter<>(change -> pattern.matcher(change.getControlNewText()).matches() ? change : null);
        this.setTextFormatter(formatter);
    }
}
