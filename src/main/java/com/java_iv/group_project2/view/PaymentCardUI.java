package com.java_iv.group_project2.view;

import javafx.scene.text.Text;

public class PaymentCardUI extends CardUI{
    private Text securityCode;
    private Text bankName;
    private Text funds;
    public PaymentCardUI(String cardHolder, String cardNum, String expiryDate, String securityCode, String bankName, String funds) {
        super(cardHolder, cardNum, expiryDate);
        this.securityCode = new Text(securityCode);
        this.bankName = new Text(bankName);
        this.funds = new Text(funds);
    }


    /**
     * Getters and setters
     */
    public String getSecurityCode() {
        return securityCode.getText();
    }

    public String getBankName() {
        return bankName.getText();
    }

    public String getFunds() {
        return funds.getText();
    }

    public void setFunds(double funds) {
        this.funds.setText(String.format("%,.2f", funds));
    }
}
