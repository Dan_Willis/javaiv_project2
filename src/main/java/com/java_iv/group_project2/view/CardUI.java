package com.java_iv.group_project2.view;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class CardUI extends WalletItemUI{
    private Text cardHolder;
    private Text cardNum;
    private Text expiryDate;

    public CardUI(String cardHolder, String cardNum) {
        this(cardHolder, cardNum, "");
    }

    public CardUI(String cardHolder, String cardNum, String expiryDate) {
        this.cardNum = new Text(cardNum);
        this.cardNum.setFill(Paint.valueOf("#0e2648"));
        this.cardNum.getStyleClass().add("card-num");
        this.cardNum.setStyle("-fx-font-weight: 700;");
        this.cardHolder = new Text(cardHolder);
        this.expiryDate = new Text(expiryDate);
        this.getChildren().addAll(this.cardNum);
    }


    /**
     * mutators and accessors
     */
    public void setCardNum(String cardNum) {
        this.cardNum.setText(cardNum);
    }


    public String getCardHolder() {
        return cardHolder.getText();
    }

    public String getExpiryDate() {
        return expiryDate.getText();
    }

    @Override
    public int getItemId() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void updateText(String newText) {
        // TODO Auto-generated method stub
        this.cardNum.setText(newText);
        
    }

    @Override
    public String toString() {
        return this.cardNum.getText();
    }
}
