package com.java_iv.group_project2.view;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Deck extends BorderPane {
    private VBox deck;
    private HBox bottomBox;
    private Text message;
    private Button addButton;
    private ScrollPane scrollBox;
    private Label label;
    private final String ADD_BTN_SYMBOL = "+";

    private WalletItemUI currentSelected;

    public Deck(String title) {
        this.getStyleClass().add("deck");
        this.label = new Label(title);
        this.deck = new VBox(10);
        this.scrollBox = new ScrollPane();
        this.scrollBox.setStyle("-fx-background-color: transparent;" +
                "-fx-border-radius: 5;");
        this.scrollBox.setPrefHeight(400);
        this.scrollBox.setContent(this.deck);
        this.scrollBox.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        this.scrollBox.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        this.bottomBox = new HBox(10);
        this.message = new Text("Add");
        this.message.setFill(Paint.valueOf("#CED9F9FF"));
        this.addButton = new Button(ADD_BTN_SYMBOL);
        this.addButton.setPadding(new Insets(3,15,3,15));

        this.bottomBox.getChildren().addAll(this.addButton, this.message);

        this.setTop(this.label);
        this.setCenter(this.scrollBox);
        this.setBottom(this.bottomBox);

        BorderPane.setMargin(this.bottomBox, new Insets(10, 0, 0, 0));
        BorderPane.setAlignment(this.bottomBox, Pos.BOTTOM_LEFT);
        BorderPane.setAlignment(this.label, Pos.CENTER);
        this.setPrefWidth(203);




    }

    /**
     * returns a list of walletitemUIs
     * @return list
     */
    public List<WalletItemUI> iterator(){
        List<WalletItemUI> list = new ArrayList<>();
        for (Node item : this.deck.getChildren()) {
            WalletItemUI itemUI = ((WalletItemUI)item);
            list.add(itemUI);
        }
        return list;
    }

    /**
     * Given an id, selects the right card in the deck
     * @param id
     */
    public void makeSelected(int id){
        if(this.currentSelected != null){
            this.currentSelected.makeSelected(false);
        }

        for (WalletItemUI itemUI : this.iterator()) {
            if(itemUI.getItemId() == id){
                itemUI.makeSelected(true);
                this.currentSelected = itemUI;
                return;
            }
        }

        throw new IllegalArgumentException("while trying to make walletItem selected: id not found");
    }

    public void addWalletItem(WalletItemUI item) {
        this.deck.getChildren().add(item);
    }

    public int getDeckSize(){
        return this.deck.getChildren().size();
    }

    /**
     * gets the id of the first item
     * @return
     */
    public int getIdFirstItem(){
        if(getDeckSize() == 0){
            return -1;
        }
        return this.iterator().get(0).getItemId();
    }

    /**
     * removes an item given its an id
     * @param id
     */
    public void removeWalletItem(int id) {
        
        if(getDeckSize() == 0){
            throw new IllegalArgumentException("Cant remove from an empty deck");
        }
        for (WalletItemUI itemUI : this.iterator()) {
            if(itemUI.getItemId() == id){
                this.deck.getChildren().remove(itemUI);
                return;
            }
        }

        throw new IllegalArgumentException("id not found");
    }

    /**
     * removes all items
     * @param id
     */
    public void removeAll() {
        
        this.deck.getChildren().removeAll();
    }

    /**
     * Updates a card with a new text given its id
     * @param id
     * @param newText
     */
    public void updateItemText(int id, String newText) {
        
        if(getDeckSize() == 0){
            throw new IllegalArgumentException("Cant update from an empty deck");
        }
        for (WalletItemUI itemUI : this.iterator()) {
            if(itemUI.getItemId() == id){
                itemUI.updateText(newText);;
                return;
            }
        }

        throw new IllegalArgumentException("id not found");
    }

    /**
     * Setters and getters
     */
    public VBox getDeck() {
        return deck;
    }

    public Text getMessage() {
        return message;
    }

    public void setMessage(Text message) {
        this.message = message;
    }

    public Button getAddButton() {
        return addButton;
    }

    public void setAddButton(Button addButton) {
        this.addButton = addButton;
    }

    @Override
    public String toString() {
        return this.label.getText();
    }
}
