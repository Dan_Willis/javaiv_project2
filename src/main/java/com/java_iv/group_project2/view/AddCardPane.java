package com.java_iv.group_project2.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import java.util.regex.Pattern;

public class AddCardPane extends GridPane {
    private Label cardHolder;
    private Label cardNumber;
    private Label expiryDate;
    private Label securityCode;
    private Label bankName;
    private Label funds;
    private TextField cardHolderField;
    private TextField cardNumberField;
    private TextField expiryYearField;
    private TextField expiryMonthField;
    private TextField bankNameField;
    private TextField fundsField;
    private Text warningMessage;
    private HBox expiryDateField;
    private TextField securityCodeField;
    private Button addButton;
    private Button cancelButton;
    private HBox buttons;

    public AddCardPane() {
        this.getStyleClass().add("add-card-pane");
        // Grid Pane styling
        this.setPadding(new Insets(10, 0, 0, 0));
        this.setHgap(10);
        this.setVgap(5);
        this.setMaxWidth(500);

        this.cardHolder = new Label("Cardholder Name");
        GridPane.setHalignment(cardHolder, HPos.RIGHT);
        this.cardHolderField = new TextField();

        this.cardNumber = new Label("Card Number");
        GridPane.setHalignment(cardNumber, HPos.RIGHT);
        this.cardNumberField = new TextField();

        this.expiryDate = new Label("Expiry Date");
        GridPane.setHalignment(expiryDate, HPos.RIGHT);
        this.expiryDateField = new HBox();

        this.securityCode = new Label("Security Code");
        this.securityCodeField = new TextField();

        this.addButton = new Button("Add");
        this.cancelButton = new Button("Cancel");

        this.add(cardHolder, 0, 0);
        this.add(cardHolderField, 1, 0);

        this.add(cardNumber, 0, 1);
        this.add(cardNumberField, 1, 1);

        this.add(expiryDate, 0, 2);
        this.add(expiryDateField, 1, 2);
        this.expiryMonthField = new TextField();
        this.expiryMonthField.setMaxWidth(75);
        this.expiryMonthField.setPromptText("MM");
        this.expiryYearField = new TextField();
        this.expiryYearField.setMaxWidth(75);
        this.expiryYearField.setPromptText("YYYY");
        this.expiryDateField.getChildren().addAll(expiryMonthField, expiryYearField);

        this.bankName = new Label("Bank Name");
        GridPane.setHalignment(bankName, HPos.RIGHT);
        this.funds = new Label();
        GridPane.setHalignment(funds, HPos.RIGHT);
        this.bankNameField = new TextField();
        this.fundsField = new TextField();
        this.fundsField.setTextFormatter(getCustomTextFormat("^\\d*|^\\d+\\.\\d{0,2}"));


        this.expiryMonthField.setTextFormatter(getCustomTextFormat("\\d{0,2}"));
        this.expiryYearField.setTextFormatter(getCustomTextFormat("\\d{0,4}"));
        this.cardNumberField.setTextFormatter(getCustomTextFormat("\\d{0,16}"));
        this.securityCodeField.setTextFormatter(getCustomTextFormat("\\d{0,3}"));

        this.warningMessage = new Text();
        this.warningMessage.setFill(Paint.valueOf("#CED9F9FF"));
        this.buttons = new HBox(5);
        this.buttons.getChildren().addAll(this.addButton, this.cancelButton, this.warningMessage);
        this.add(buttons, 0, 3, 4, 1);
        GridPane.setHalignment(buttons, HPos.RIGHT);

    }

    /**
     * Returns a TextFormatter object based on the given regex string
     */
    public TextFormatter getCustomTextFormat(String regexp) {
        Pattern pattern = Pattern.compile(regexp);
        return new TextFormatter<>(change -> pattern.matcher(change.getControlNewText()).matches() ? change : null);
    }

    /**
     * Sets the styling of the pane for adding payment cards
     */
    public void setStylePayment(String fundsLabel) {
        this.add(securityCode, 2, 0);
        this.add(securityCodeField, 3, 0);
        this.add(bankName, 2, 1);
        this.add(bankNameField, 3, 1);
        this.add(funds, 2, 2);
        funds.setText(fundsLabel);
        this.add(fundsField, 3, 2);
        this.securityCodeField.setMaxWidth(50);
        this.setMaxWidth(570);
    }

    /**
     * Resets the styling of the pane
     */
    public void resetStyle() {
        this.getChildren().remove(securityCodeField);
        this.getChildren().remove(securityCode);
        this.getChildren().remove(bankNameField);
        this.getChildren().remove(bankName);
        this.getChildren().remove(funds);
        this.getChildren().remove(fundsField);
        this.setMaxWidth(500);
        this.cardNumberField.clear();
        this.securityCodeField.clear();
        this.expiryYearField.clear();
        this.expiryMonthField.clear();
        this.cardHolderField.clear();
        this.bankNameField.clear();
        this.fundsField.clear();
    }


    /**
     * Mutators and accessors
     */
    public Button getAddButton() {
        return addButton;
    }

    public Button getCancelButton() {
        return cancelButton;
    }

    public TextField getCardHolderField() {
        return cardHolderField;
    }

    public void setCardHolderField(TextField cardHolderField) {
        this.cardHolderField = cardHolderField;
    }

    public TextField getCardNumberField() {
        return cardNumberField;
    }

    public void setCardNumberField(TextField cardNumberField) {
        this.cardNumberField = cardNumberField;
    }

    public TextField getExpiryYearField() {
        return expiryYearField;
    }

    public void setExpiryYearField(TextField expiryYearField) {
        this.expiryYearField = expiryYearField;
    }

    public TextField getExpiryMonthField() {
        return expiryMonthField;
    }

    public void setExpiryMonthField(TextField expiryMonthField) {
        this.expiryMonthField = expiryMonthField;
    }

    public Text getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage.setText(warningMessage);
    }

    public TextField getSecurityCodeField() {
        return securityCodeField;
    }

    public void setSecurityCodeField(TextField securityCodeField) {
        this.securityCodeField = securityCodeField;
    }

    public TextField getBankNameField() {
        return bankNameField;
    }

    public void setBankNameField(TextField bankNameField) {
        this.bankNameField = bankNameField;
    }

    public TextField getFundsField() {
        return fundsField;
    }

    public void setFundsField(TextField fundsField) {
        this.fundsField = fundsField;
    }
}
