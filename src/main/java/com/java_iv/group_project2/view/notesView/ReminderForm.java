package com.java_iv.group_project2.view.notesView;

import java.util.regex.Pattern;

import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Represents the from for setting the frequency of reminders
 */
public class ReminderForm extends VBox {
    private CheckBox reminderToggle;
    private boolean isReminderActive;

    private boolean isFrequencyValid = false;
    private int minutes = 0;
    private int hours = 0;
    private int days = 0;

    private TextField minuteSelector;
    private TextField hourSelector;
    private TextField daySelector;

    private VBox formVBox;
    private Label lblMinute;
    private Label lblHour;
    private Label lblDay;

    private Text messageText;

    public ReminderForm(){
        this.getStyleClass().add("reminder-form");
        this.reminderToggle = new CheckBox("reminder toggle");
        //reminderToggle.getStyleClass().add("notification");
        this.reminderToggle.setOnAction(e -> this.onToggle());
        this.isReminderActive = false;
        
        this.minuteSelector = new TextField("0");
        this.hourSelector = new TextField("0");
        this.daySelector = new TextField("0");
        this.setTextFormatters();

        this.messageText = new Text();

        this.setTimeSelectorActions();

        this.lblMinute = new Label("Enter the number of minutes", this.minuteSelector);
        lblMinute.setContentDisplay(ContentDisplay.BOTTOM);
        this.lblHour = new Label("Enter the number of hours", this.hourSelector);
        lblHour.setContentDisplay(ContentDisplay.BOTTOM);
        this.lblDay = new Label("Enter the number of days", this.daySelector);
        lblDay.setContentDisplay(ContentDisplay.BOTTOM);
        this.formVBox = new VBox(lblMinute, lblHour, lblDay, messageText);

        this.getChildren().addAll(reminderToggle, this.formVBox);

    }

    /**
     * It shows and hides the form depending on the whether the reminders are active or not
     */
    private void showToggle(){
        if(this.isReminderActive){
            this.formVBox.setVisible(true);  
            this.showIsFrequencyValid();      

        } else {
            this.minuteSelector.setText("0");
            this.hourSelector.setText("0");
            this.daySelector.setText("0");
            this.formVBox.setVisible(false); 
        }
    }

    /**
     * sets reminder state (active or not) when the toggle checkbox is clicked
     */
    private void onToggle(){
        this.isReminderActive = this.reminderToggle.isSelected();
        showToggle();
    }

    /**
     * adds validation when textfields are modified
     */
    private void setTimeSelectorActions(){
        this.minuteSelector.textProperty().addListener(o -> this.showIsFrequencyValid());
        this.hourSelector.textProperty().addListener(o -> this.showIsFrequencyValid());
        this.daySelector.textProperty().addListener(o -> this.showIsFrequencyValid());
    }

    /**
     * sets the formatters for the reminder form (unable to type none number characters)
     */
    private void setTextFormatters(){
        Pattern pattern = Pattern.compile("\\d*(\\.\\d{0,2})?");
        TextFormatter<String>  minuteFormatter = new TextFormatter<>(change -> pattern.matcher(change.getControlNewText()).matches() ? change : null);
        this.minuteSelector.setTextFormatter(minuteFormatter);
        TextFormatter<String>  hourFormatter = new TextFormatter<>(change -> pattern.matcher(change.getControlNewText()).matches() ? change : null);
        this.hourSelector.setTextFormatter(hourFormatter);
        TextFormatter<String>  dayFormatter = new TextFormatter<>(change -> pattern.matcher(change.getControlNewText()).matches() ? change : null);
        this.daySelector.setTextFormatter(dayFormatter);
    }

    public boolean getIsReminderActive(){
        return this.isReminderActive;
    }

    /**
     * sets isReminderActive and changes the controles accordingly 
     * @param isReminderActive
     */
    public void setIsReminderActive(boolean isReminderActive){
        this.isReminderActive = isReminderActive;
        this.reminderToggle.setSelected(isReminderActive);
        this.showToggle();
    }

    /**
     * syncs isReminderActive with isFrequencyValid and changes the controles accordingly
     */
    public void syncReminderActiveFrequencyValid(){
        this.setIsReminderActive(getIsFrequencyValid());
    }

    public boolean getIsFrequencyValid(){
        return this.isFrequencyValid;
    }

     /**
     * gets frequency in minutes
     * @return frequency
     */
    public int getFrequency(){
        return this.minutes + this.hours*60 + this.days*60*24;
    }

    /**
     * disables and enables the whole reminder form
     * @param isDisabled
     */
    public void setReminderFormDisable(boolean isDisabled){
        this.reminderToggle.setSelected(false);
        this.isReminderActive = false;
        this.showToggle();
        this.reminderToggle.setDisable(isDisabled);
    }

    /**
     * shows to the user if the frequency inputed is valid
     */
    private void showIsFrequencyValid() {
        
        try {
            this.minutes = Integer.parseInt(this.minuteSelector.getText());
            this.hours = Integer.parseInt(this.hourSelector.getText());
            this.days = Integer.parseInt(this.daySelector.getText());
            if (this.getFrequency() > 0) {
                this.messageText.setText("Valid frequency!");
                this.messageText.setStyle("-fx-fill: green;");
                this.isFrequencyValid = true;

            } else {
                this.messageText.setText("The total sum has to be greater than zero.");
                this.messageText.setStyle("-fx-fill: red;");
                this.isFrequencyValid = false;
            }

        } catch (NumberFormatException e) {
            this.messageText.setText("Fields can't be left empty.");
            this.messageText.setStyle("-fx-fill: red;");
            this.isFrequencyValid = false;
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
        this.minuteSelector.setText("" + minutes);
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
        this.hourSelector.setText("" + hours);
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
        this.daySelector.setText("" + days);
    }

}
