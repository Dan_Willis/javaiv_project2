package com.java_iv.group_project2.view.notesView;

import com.java_iv.group_project2.view.WalletItemUI;

import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * represents a note card displayed in the deck
 * manages its own notifications
 */
public class NoteUI extends WalletItemUI{
    private Text title;
    private int noteId;
    private ScheduledService<Boolean> service = null;
    private int nbNotifications = -1;
    private int currentNotifFrequency;

    public NoteUI(String title, int id) {
        this.title = new Text(title);
        this.noteId = id;
        this.getChildren().addAll(this.title);
    }

    /**
     * adds and starts a notification
     * will not add if frequency is the same
     * @param frequency
     * @param notificationText
     */
    public void addNotification(int frequency, Text notificationText){
        if(frequency == this.currentNotifFrequency){
            return;
        }
        this.currentNotifFrequency = frequency;
        if (this.service != null){
            this.service.cancel();
            this.nbNotifications = -1;
        }
        
        this.service = new ScheduledService<Boolean>() {
            protected Task<Boolean> createTask() {
                return new Task<Boolean>() {
                    protected Boolean call() {
                       
                        return true;
                    }
                };
            }
        };
        service.setPeriod(Duration.minutes(frequency));
        service.setOnSucceeded(
            e -> {
                if(this.nbNotifications == -1){
                    this.nbNotifications++;
                    return;
                }
                String suffix = "";
                if(this.nbNotifications > 0){
                    suffix = " (" + this.nbNotifications + ")";
                }
                this.nbNotifications++;
                notificationText.setText("Latest reminder: " + this.title.getText() + suffix);
            }
        );

        service.setOnRunning(e -> System.out.println("running: " + this.title.getText()));

        service.start();
        
    }

    /**
     * stops and deletes the notification
     */
    public void stopNotification(){
        if(this.service != null){
            this.service.cancel();
            this.service = null;
        }
        this.nbNotifications = -1;
        this.currentNotifFrequency = 0;
    }

    public int getItemId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public void updateText(String newTitle){
        this.title.setText(newTitle);
    }

    @Override
    public String toString() {
        return this.title.getText();
    }
}
