package com.java_iv.group_project2.view.notesView;

import com.java_iv.group_project2.controller.Controller;
import com.java_iv.group_project2.model.WalletItem;
import com.java_iv.group_project2.view.Deck;
import com.java_iv.group_project2.view.WalletItemUI;

import javafx.animation.PauseTransition;
import javafx.animation.RotateTransition;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * represents where the notes are displayed and edited
 */
public class NoteView extends HBox {
    private Controller controller;
    private int currentNoteId;

    private TextField tfTitle;
    private Label lblTitle;
    private Button btnSave;
    private Button btnDelete;
    private HBox topBar;

    private TextArea taNoteDisplay;
    private Text messageText;
    private boolean isMessageAnimPlaying;
    private VBox displayArea;

    private ReminderForm reminderForm;
    private NoteDeck noteDeck;

    public NoteView(Controller controller, Text notificationText){
        this.getStyleClass().add("note-view");
        this.setPadding(new Insets(11.5, 12.5, 13.5, 14.5));
        this.controller = controller;
        this.tfTitle = new TextField();
        this.lblTitle = new Label("Title: ", this.tfTitle);
        this.lblTitle.setContentDisplay(ContentDisplay.RIGHT);
        //lblTitle.getStyleClass().add("notification");

        this.btnSave = new Button("Save");
        this.btnDelete = new Button("Delete");
        this.topBar = new HBox(lblTitle, btnSave, btnDelete);
        this.topBar.setSpacing(10);

        this.taNoteDisplay = new TextArea("Enter notes here...");
        this.taNoteDisplay.setMaxWidth(600);
        this.messageText = new Text();
        this.messageText.textProperty().addListener(o -> playAnim(this.messageText));

        this.displayArea = new VBox(this.topBar, this.taNoteDisplay, this.messageText);
        this.displayArea.setSpacing(15);

        this.noteDeck = new NoteDeck("Notes", notificationText);
        this.reminderForm = new ReminderForm();
        this.getChildren().addAll( 
            this.noteDeck, 
            new Separator(Orientation.VERTICAL),
            this.displayArea,
            new Separator(Orientation.VERTICAL),
            this.reminderForm
        );

        this.setSpacing(25);

        setEventHandlers();
        setEditable();
    }

    /**
     * if there is not current not (id < 1) will disable the form for editing notes
     */
    private void setEditable(){
        if(this.currentNoteId < 1){
            this.tfTitle.setText("");
            this.lblTitle.setDisable(true);
            this.taNoteDisplay.setDisable(true);
            this.taNoteDisplay.setText("");
            this.btnSave.setDisable(true);
            this.btnDelete.setDisable(true);

            this.reminderForm.setReminderFormDisable(true);
        } else {
            this.lblTitle.setDisable(false);
            this.taNoteDisplay.setDisable(false);
            this.btnSave.setDisable(false);
            this.btnDelete.setDisable(false);
            this.reminderForm.setReminderFormDisable(false);
        }
    }
    
    /**
     * sets event handlers for the buttons in note view
     */
    private void setEventHandlers(){
        this.btnSave.setOnAction(e -> saveNote(this.currentNoteId));
        this.btnDelete.setOnAction(e -> deleteNote(this.currentNoteId));
        this.noteDeck.getAddButton().setOnAction(e -> createNote());
    }

    /**
     * saves an existing note
     * @param id
     */
    private void saveNote(int id){
        // do some validaton
        // call method in controler
        // update
        if(reminderForm.getIsFrequencyValid() && reminderForm.getIsReminderActive()){
            this.controller.updateNote(
                id, 
                tfTitle.getText(), 
                taNoteDisplay.getText(), 
                reminderForm.getMinutes(), 
                reminderForm.getHours(), 
                reminderForm.getDays()
            );
            messageText.setText("Saved!");
            messageText.setStyle("-fx-fill: green;");
            this.noteDeck.updateNotification(id, reminderForm.getFrequency());
        } else {
            this.controller.updateNote(
                id, 
                tfTitle.getText(), 
                taNoteDisplay.getText(), 
                0,
                0,
                0
            );
            if(!reminderForm.getIsFrequencyValid()){
                messageText.setText("Reminder Form invalid, did not save it but saved the rest.");
                messageText.setStyle("-fx-fill: red;");
            }
            if(!reminderForm.getIsReminderActive()){
                messageText.setText("Saved!");
                messageText.setStyle("-fx-fill: green;");
            }
            this.noteDeck.updateNotification(id, 0);
        }

        this.noteDeck.updateItemText(id, tfTitle.getText());
        
        this.reminderForm.syncReminderActiveFrequencyValid();
        
    }

    
    /**
     * plays a rotate animation
     * 
     * @param messageText
     */
    private void playAnim(Text messageText) {
        if(!this.isMessageAnimPlaying){
            this.isMessageAnimPlaying = true;
            RotateTransition rt = new RotateTransition(Duration.millis(100), messageText);
            rt.setByAngle(10);
            rt.setAutoReverse(true);
            rt.setCycleCount(4);
            rt.play();
            this.isMessageAnimPlaying = false;
        }
        
    }

    /**
     * deletes an exiting note
     * @param id
     */
    private void deleteNote(int id){
        // ATTENTION: (ORDER MATTERS) you have to remove from the deck before you view the first item of the deck
        this.controller.deleteNote(id);
        this.noteDeck.updateNotification(id, 0);
        this.noteDeck.removeWalletItem(id);
        this.viewNote(this.noteDeck.getIdFirstItem());
        setEditable();
    }

    /**
     * creates an empty note
     */
    private void createNote(){
        
        // give me back the id of the new note
        // call viewNote()

        int id = this.controller.createNote();

        if(id == -1){
            messageText.setText("Cannot have more than 10 notes: note not created.");
            messageText.setStyle("-fx-fill: red;");
            return;
        }
    
        addNote(id);

    }

    /**
     * empties the noteview
     */
    public void empty(){
        
        for (WalletItemUI item : this.noteDeck.iterator()) {
            this.deleteNote(item.getItemId());
        }
    }

    /**
     * adds a an existing note in the model to the view
     * @param id
     */
    public void addNote(int id){
        NoteUI item = new NoteUI(this.controller.getNoteTitle(id), id);
        item.setOnMouseClicked(e -> viewNote(id));
        this.noteDeck.addWalletItem(item);
        this.viewNote(id);
        setEditable();

        messageText.setText("Note created!");
        messageText.setStyle("-fx-fill: #56D3BF;");
    }

    /**
     * adds the info of an existing form to the note editor to display it
     * @param id
     */
    private void viewNote(int id){
    
        this.currentNoteId = id;
        if(currentNoteId < 1){
            messageText.setText("No notes available");
            messageText.setStyle("-fx-fill: #CED9F9;");
            return;
        }
        this.controller.viewNote(id, this.tfTitle, this.taNoteDisplay, this.reminderForm);
        this.noteDeck.makeSelected(id);
        System.out.println("id: " + id);
    }
   
   
}
