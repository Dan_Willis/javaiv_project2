package com.java_iv.group_project2.view.notesView;

import java.util.ArrayList;
import java.util.List;


import com.java_iv.group_project2.view.Deck;
import com.java_iv.group_project2.view.WalletItemUI;

import javafx.scene.Node;
import javafx.scene.text.Text;

/**
 * represents a deck of notes
 */
public class NoteDeck extends Deck {
    private Text notificationText;

    public NoteDeck(String title, Text notificationText){
        super(title);
        this.notificationText = notificationText;
    }

    /**
     * updates a notification with a new frequency
     * if the frequency is 0 stops the notification
     * @param id
     * @param newFrequency
     */
    public void updateNotification(int id, int newFrequency) {
        
        if(getDeckSize() == 0){
            throw new IllegalArgumentException("Cant update from an empty deck");
        }
        for (WalletItemUI itemUI : this.iterator()) {
            System.out.println(itemUI.getItemId());
            if(itemUI.getItemId() == id){
                if (newFrequency == 0){
                    ((NoteUI)itemUI).stopNotification();
                } else {
                    ((NoteUI)itemUI).addNotification(newFrequency, notificationText);
                }
                
                return;
            }
        }

        throw new IllegalArgumentException("id not found");
    }

}
