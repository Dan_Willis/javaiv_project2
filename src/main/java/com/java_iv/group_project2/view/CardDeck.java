package com.java_iv.group_project2.view;

import javafx.scene.Node;

import java.util.ArrayList;
import java.util.List;

public class CardDeck extends Deck{
    private CardUI selectedCard;

    public CardDeck(String title) {
        super(title);
    }

    /**
     * Returns a list of CardUI from the deck
     */
    private List<CardUI> cardIterator(){
        List<CardUI> list = new ArrayList<>();
        for (Node item : this.getDeck().getChildren()) {
            CardUI itemUI = ((CardUI)item);
            list.add(itemUI);
        }
        return list;
    }

    /**
     * Sets the selected card based on card number
     */
    public void makeSelected(String cardNum){
        if(this.selectedCard != null){
            this.selectedCard.makeSelected(false);
        }

        for (CardUI card: this.cardIterator()) {
            if(String.valueOf(card).equals(cardNum)){
                card.makeSelected(true);
                this.selectedCard = card;
                return;
            }
        }

        throw new IllegalArgumentException("Card Number not found");
    }

    public CardUI getSelectedCard() {
        return selectedCard;
    }
}
