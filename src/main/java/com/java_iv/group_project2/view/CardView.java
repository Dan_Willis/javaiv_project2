package com.java_iv.group_project2.view;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class CardView extends GridPane {
    private CardPane cardPane;
    private InfoPane cardInfo;
    private CashPane cashPane;
    private AddCardPane addCardPane;
    private String deckToAdd;

    public CardView() {
        this.setAlignment(Pos.CENTER);
        this.setPadding(new Insets(11.5, 12.5, 13.5, 14.5));
        this.setHgap(25);
        this.setVgap(5.5);


        this.cardPane = new CardPane();
        this.cardInfo = new InfoPane();
        this.cashPane = new CashPane();
        this.addCardPane = new AddCardPane();

        this.add(cardPane, 0, 0);
        this.add(cardInfo, 1, 0);
        this.add(addCardPane, 0, 1);
        this.add(cashPane, 1, 1);
        GridPane.setMargin(cashPane, new Insets(15, 0, 0, 0));
        GridPane.setHalignment(cashPane, HPos.CENTER);

        GridPane.setHalignment(addCardPane, HPos.CENTER);

        this.addCardPane.setVisible(false);

        this.setAddButtons();
        this.addCardPane.getCancelButton().setOnAction(e -> hideAddCardPane());
        this.resetCardDetails();
    }

    /**
     * Adds the event handlers for the deck buttons for each card type
     */
    public void setAddButtons() {
        this.getCardPane().getDebitCardBtn().setOnAction(e -> showAddCardPane((Button) e.getSource()));
        this.getCardPane().getCreditCardBtn().setOnAction(e -> showAddCardPane((Button) e.getSource()));
        this.getCardPane().getPersonalCardBtn().setOnAction(e -> showAddCardPane((Button) e.getSource()));
    }

    /**
     * Sets the form to add a new card to be visible
     */
    public void showAddCardPane(Button button) {
        this.addCardPane.setVisible(true);
        this.deckToAdd = String.valueOf(button.getParent().getParent());
        disableAddButtons();
        if (this.deckToAdd.equals("Debit Cards")) {
            this.addCardPane.setStylePayment("Funds");
        } else if (this.deckToAdd.equals("Credit Cards")) {
            this.addCardPane.setStylePayment("Limit");
        }
    }

    /**
     * Hides the form to add a new card
     */
    public void hideAddCardPane() {
        this.addCardPane.resetStyle();
        this.addCardPane.setVisible(false);
        enableAddButtons();
        this.deckToAdd = "";
        this.addCardPane.setWarningMessage("");
        this.requestFocus();
    }

    /**
     * Disables the add card button for all the decks in the card pane
     */
    public void disableAddButtons() {
        this.getCardPane().getDebitCardBtn().setDisable(true);
        this.getCardPane().getCreditCardBtn().setDisable(true);
        this.getCardPane().getPersonalCardBtn().setDisable(true);
    }

    /**
     * Enables the add card button for all the decks in the card pane
     */
    public void enableAddButtons() {
        this.getCardPane().getDebitCardBtn().setDisable(false);
        this.getCardPane().getCreditCardBtn().setDisable(false);
        this.getCardPane().getPersonalCardBtn().setDisable(false);
    }

    /**
     * Clears the card details in the info pane
     */
    public void resetCardDetails() {
        this.cardInfo.setCardNumber("");
        this.cardInfo.setCardHolderName("");
        this.cardInfo.setExpiryDate("");
        this.cardInfo.setSecurityCode("");
        this.cardInfo.setBankName("");
        this.cardInfo.setFunds("");
        this.cardInfo.setWarningText("");
        this.cardInfo.getPayField().setText("");
        this.cardInfo.getPayBtn().setVisible(false);
        this.cardInfo.getPayField().setVisible(false);
        this.cardInfo.getDeleteBtn().setVisible(false);
    }

    /**
     * Shows card details for personal cards
     */
    public void showCardDetails(CardUI card) {
        resetCardDetails();
        this.cardInfo.setCardNumber("Card Number: "+ String.valueOf(card).replaceAll("....", "$0 "));
        this.cardInfo.setCardHolderName(card.getCardHolder());
        this.cardInfo.setExpiryDate(card.getExpiryDate());
        this.cardInfo.setSecurityCode("");
        this.cardInfo.setBankName("");
        this.cardInfo.setFunds("");
        this.cardInfo.getPayBtn().setVisible(false);
        this.cardInfo.getPayField().setVisible(false);
        this.cardInfo.getDeleteBtn().setVisible(true);
    }

    /**
     * Shows card details for payment cards
     */
    public void showPaymentCardDetails(PaymentCardUI card, String fundLabel) {
        resetCardDetails();
        this.cardInfo.setCardNumber("Card Number: "+ String.valueOf(card).replaceAll("....", "$0 "));
        this.cardInfo.setCardHolderName(card.getCardHolder());
        this.cardInfo.setExpiryDate("Expiry Date: " + card.getExpiryDate());
        this.cardInfo.setSecurityCode("Security Code: " + card.getSecurityCode());
        this.cardInfo.setBankName(card.getBankName());
        this.cardInfo.setFunds(fundLabel + card.getFunds());
        this.cardInfo.getPayBtn().setVisible(true);
        this.cardInfo.getPayField().setVisible(true);
        this.cardInfo.getDeleteBtn().setVisible(true);
    }

    /**
     * Empties all the card decks inside the card pane
     */
    public void empty() {
        resetCardDetails();
        hideAddCardPane();

        // Clear credit cards
        this.cardPane.getCreditCards().getDeck().getChildren().clear();
        // Clear debit cards
        this.cardPane.getDebitCards().getDeck().getChildren().clear();
        // CLear personal cards
        this.cardPane.getPersonalCards().getDeck().getChildren().clear();

    }

    /**
     * Getters
     */
    public CardPane getCardPane() {
        return cardPane;
    }

    public InfoPane getCardInfo() {
        return cardInfo;
    }

    public CashPane getCashPane() {
        return cashPane;
    }

    public AddCardPane getAddCardPane() {
        return addCardPane;
    }

    public String getDeckToAdd() {
        return deckToAdd;
    }
}
