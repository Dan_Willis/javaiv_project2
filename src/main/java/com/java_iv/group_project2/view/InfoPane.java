package com.java_iv.group_project2.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.*;

public class InfoPane extends BorderPane {
    private VBox cardDetails;
    private Text title;
    private Text cardHolderName;
    private Text cardNumber;
    private Text securityCode;
    private Text bankName;
    private Text funds;
    private Text expiryDate;
    private Text warningText;
    private Button deleteBtn;
    private Button payBtn;
    private PaymentField payField;
    private HBox userField;

    public InfoPane() {
        // BorderPane Styling
        this.getStyleClass().add("info-pane");
        this.setPrefSize(350, 180);
        this.setPadding(new Insets(30, 5, 5, 5));


        this.cardDetails = new VBox(20);
        this.title = new Text("Card Details");
        this.title.getStyleClass().add("info-pane-title");
        this.title.setFont(Font.font(25));
        this.title.setFill(Paint.valueOf("#CED9F9FF"));

        this.cardHolderName = new Text();
        this.cardHolderName.setFont(Font.font(15));
        this.cardHolderName.setFill(Paint.valueOf("#CED9F9FF"));

        this.cardNumber = new Text();
        this.cardNumber.setFont(Font.font(15));
        this.cardNumber.setFill(Paint.valueOf("#CED9F9FF"));

        this.expiryDate = new Text();
        this.expiryDate.setFont(Font.font(15));
        this.expiryDate.setFill(Paint.valueOf("#CED9F9FF"));

        this.securityCode = new Text();
        this.securityCode.setFont(Font.font(15));
        this.securityCode.setFill(Paint.valueOf("#CED9F9FF"));

        this.bankName = new Text();
        this.bankName.setFont(Font.font(15));
        this.bankName.setFill(Paint.valueOf("#CED9F9FF"));

        this.funds = new Text();
        this.funds.setFont(Font.font(15));
        this.funds.setFill(Paint.valueOf("#CED9F9FF"));

        this.warningText = new Text();
        this.warningText.setFill(Paint.valueOf("#CED9F9FF"));
        this.warningText.setTextAlignment(TextAlignment.CENTER);

        this.deleteBtn = new Button("Delete");

        this.cardDetails.getChildren().addAll(
                this.title,
                this.cardHolderName,
                this.cardNumber,
                this.expiryDate,
                this.securityCode,
                this.bankName,
                this.funds,
                this.warningText
        );

        this.setTop(this.cardDetails);
        this.cardDetails.setAlignment(Pos.CENTER);

        this.userField = new HBox(5);
        this.payBtn = new Button("Pay");
        this.payField = new PaymentField();
        this.payField.setPromptText("Enter amount");
        this.userField.getChildren().addAll(this.payField, this. payBtn, this.deleteBtn);
        this.userField.setAlignment(Pos.BASELINE_RIGHT);
        this.setBottom(this.userField);

    }

    /**
     * Setters and getters
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber.setText(cardNumber);
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName.setText(cardHolderName);
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode.setText(securityCode);
    }

    public void setBankName(String bankName) {
        this.bankName.setText(bankName);
    }

    public void setFunds(String funds) {
        this.funds.setText(funds);
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate.setText(expiryDate);
    }

    public void setWarningText(String warningText) {
        this.warningText.setText(warningText);
    }

    public HBox getUserField() {
        return userField;
    }

    public Button getDeleteBtn() {
        return deleteBtn;
    }

    public Button getPayBtn() {
        return payBtn;
    }

    public PaymentField getPayField() {
        return payField;
    }

    public String getCardNumber() {
        return cardNumber.getText();
    }
}
