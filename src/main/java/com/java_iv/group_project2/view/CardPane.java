package com.java_iv.group_project2.view;

import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeType;

public class CardPane extends HBox {
    private CardDeck debitCards;
    private CardDeck creditCards;
    private CardDeck personalCards;
    private CardUI currentSelectedCard;

    public CardPane() {
        this.debitCards = new CardDeck("Debit Cards");

        this.creditCards = new CardDeck("Credit Cards");
        this.personalCards = new CardDeck("Personal Cards");

        this.getChildren().addAll(
                this.debitCards,
                this.creditCards,
                this.personalCards
        );

        this.setSpacing(25);
    }

    /**
     * mutators and accessors
     */
    public Button getDebitCardBtn() {
        return this.debitCards.getAddButton();
    }

    public Button getCreditCardBtn() {
        return this.creditCards.getAddButton();
    }

    public Button getPersonalCardBtn() {
        return this.personalCards.getAddButton();
    }

    public CardDeck getDebitCards() {
        return debitCards;
    }

    public CardDeck getCreditCards() {
        return creditCards;
    }

    public CardDeck getPersonalCards() {
        return personalCards;
    }

    public CardUI getCurrentSelectedCard() {
        return currentSelectedCard;
    }

    /**
     * Sets and modifies the styling of the current selected card
     */
    public void setCurrentSelectedCard(CardUI currentSelectedCard) {
        CardUI previousCard = this.currentSelectedCard;
        if (currentSelectedCard == null) {
            this.currentSelectedCard = null;
        } else {
            this.currentSelectedCard = currentSelectedCard;
            if (previousCard != null) {
                previousCard.getItem().setStroke(Color.valueOf("transparent"));
                previousCard.getItem().setStrokeWidth(0.0);
                previousCard.getItem().setStrokeType(StrokeType.INSIDE);
            }
            currentSelectedCard.getItem().setStroke(Color.valueOf("#097969"));
            currentSelectedCard.getItem().setStrokeWidth(3.0);
            currentSelectedCard.getItem().setStrokeType(StrokeType.INSIDE);
        }


    }
}
