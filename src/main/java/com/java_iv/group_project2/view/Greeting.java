package com.java_iv.group_project2.view;

import javafx.application.Platform;
import javafx.scene.text.Text;

/**
 * runnable that greets the user
 */
public class Greeting implements Runnable{
    private Text messageText;
    private MainView view;
    public Greeting(Text messageText, MainView view){
        this.messageText = messageText;
        this.view = view;
    }
    @Override
    public void run() {
        int num = 10000000;
        for (int i = 0; i < num; i++) {
            System.out.println(i);

            if (i == (int)num/3){
                Platform.runLater(() -> {
                    this.messageText.setText("Greetings: let's make this right");
                });
            }

            if (i == (int)num/2){
                Platform.runLater(() -> {
                    this.messageText.setText("Greetings: Half Way There!");
                });
            }

            if (i == (int)num-(num/3)){
                Platform.runLater(() -> {
                    this.messageText.setText("Greetings: Almost done!");
                });
            }

        }
        Platform.runLater(() -> {
            messageText.setText("Greetings Done!");
            view.stopGreetingAnim();
        });
    }
}
