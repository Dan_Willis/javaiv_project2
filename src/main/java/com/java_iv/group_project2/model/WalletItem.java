package com.java_iv.group_project2.model;

/**
 * Empty interface to mark the sub-classes as an item of the Wallet
 */
public interface WalletItem {
    
}
