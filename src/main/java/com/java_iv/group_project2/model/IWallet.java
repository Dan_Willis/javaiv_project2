package com.java_iv.group_project2.model;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents functionality to implement when making a wallet
 */
public interface IWallet {
    /**
     * Add a debit card to the e-wallet
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @param securityCode
     * @param bankName
     * @param funds
     * @return
     */
    public boolean addCard(String cardHolderName, String cardNumber, Date date, int securityCode, String bankName, double funds) throws Exception;
    
    /**
     * Add a credit card to the e-wallet
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @param securityCode
     * @param bankName
     * @param limit
     * @return
     */
    public boolean addCard(String cardHolderName, String cardNumber, Date date, int securityCode, String bankName, int limit) throws Exception;

    /**
     * Add a personal card with date to the e-wallet
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @return
     */
    public boolean addCard(String cardHolderName, String cardNumber, Date date) throws Exception;

    /**
     * Add a personal card without date to the e-wallet
     * @param cardHolderName
     * @param cardNumber
     * @return
     */
    public boolean addCard(String cardHolderName, String cardNumber) throws Exception;
    
    /**
     * Add a note to the e-wallet
     * @return
     */
    public int addNote();

    /**
     * Returns a list of credit card
     * @return
     */
    public List<CreditCard> listOfCreditCards();

    /**
     * Returns a list of debit card
     * @return
     */
    public List<DebitCard> listOfDebitCards();

    /**
     * Returns a list of personal card
     * @return
     */
    public List<PersonalCard> listOfPersonalCards();

     /**
     * Returns list of notes
     * @return
     */
    public List<Note> listOfNotes();

    /**
     * Removes a card in the e-wallet
     * @param cardNumber
     */
    public void removeCard(String cardNumber);

    /**
     * Removes a note in the e-wallet
     * @param noteId
     */
    public void removeNote(int noteId);
}
