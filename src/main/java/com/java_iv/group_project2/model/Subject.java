package com.java_iv.group_project2.model;

import com.java_iv.group_project2.view.Observer;

public interface Subject {
    void register(Observer o);
    void unregister(Observer o);
    void notifyObserver();
}
