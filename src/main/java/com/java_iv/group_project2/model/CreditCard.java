package com.java_iv.group_project2.model;
import java.util.Date;

/**
 * This class creates a credit card
 */
public class CreditCard extends PaymentCard{
    private CreditCardIssuer account;

    /**
     * Constructor that use the fields of PaymentCard class and add bank account
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @param securityCode
     * @param bankName
     * @param limit
     */
    public CreditCard(String cardHolderName, String cardNumber, Date date, int securityCode, String bankName, int limit){
        super(cardHolderName, cardNumber, date, securityCode);
        this.account = new CreditCardIssuer(bankName, limit);
    }

    /**
     * Spends money with the credit card
     * @param amount
     */
    public boolean spend(double amount) throws Exception{
        return this.account.spend(amount);
    }

    /**
     * Returns the name of the bank
     * @return
     */
    public String getBankName() {
        return this.account.getBankName();
    }

    /**
     * Returns the limit of the card linked to the bank account
     * @return
     */
    public int getLimit() {
        return this.account.getLimit();
    }

    public CreditCardIssuer getAccount() {
        return account;
    }
}
