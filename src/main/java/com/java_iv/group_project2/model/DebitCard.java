package com.java_iv.group_project2.model;
import java.util.Date;

/**
 * This class creates a debit card
 */
public class DebitCard extends PaymentCard{
    private DebitCardIssuer bankAccount; 
    
    /**
     * Constructor that use the field of PaymentCard class and add a Bank Account
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @param securityCode
     * @param bankName
     * @param funds
     */
    public DebitCard(String cardHolderName, String cardNumber, Date date, int securityCode, String bankName, double funds){
        super(cardHolderName, cardNumber, date, securityCode);
        this.bankAccount = new DebitCardIssuer(bankName, funds);
    }

    /**
     * Check if the account balance is greater than or equal to the amount;
     * @param amount
     * @return
     */
    private boolean checkFundsAvailability(double amount){
        if (this.bankAccount.getBalance() >= amount){
            return true;
        }
        return false;
    }

    /**
     * Withdraw an amount from the bank account and return true if the operation is successful
     * @param amount
     * @return
     */
    public boolean withdraw(double amount) throws Exception{
        if (checkFundsAvailability(amount)){
            this.bankAccount.withdraw(amount);
            return true;
        }
        return false;
    }

    /**
     * Returns the bank name
     * @return
     */
    public String getBankName() {
        return this.bankAccount.getBankName();
    }

    /**
     * Returns the funds in the bank account
     * @return
     */
    public double getFunds() {
        return this.bankAccount.getBalance();
    }
}
