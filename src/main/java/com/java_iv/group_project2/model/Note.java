package com.java_iv.group_project2.model;
import java.util.Date;
// import java.util.Timer;
// import java.util.TimerTask;

/**
 * This class creates a note
 */
public class Note implements WalletItem{
    private Date creationDate;
    private String title;
    private String body;
    private boolean isReminderActive;
    private int reminderMinutes;
    private int reminderHours;
    private int reminderDays;

    public static int sequenceId = 1;
    private int noteId;

    /**
     * Constructor that automatically provides an id
     * @param body
     * @param issueReminder
     */
    public Note() {
        this.creationDate = new Date();
        this.title = "My new note";
        this.body = "Enter text here";
        this.isReminderActive = false;
        this.noteId = sequenceId++;
    }

    /**
     * Returns the creation date
     * @return
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Returns the body of the note
     * @return
     */
    public String getBody() {
        return body;
    }

    /**
     * Returns the state of the reminder
     * @return
     */
    public boolean getIsReminderActive() {
        return this.isReminderActive;
    }
    
    /**
     * Returns the id of the note
     * @return
     */
    public int getNoteId(){
        return noteId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setReminderActive(boolean isReminderActive) {
        this.isReminderActive = isReminderActive;
    }

    public int getReminderMinutes() {
        return reminderMinutes;
    }

    public void setReminderMinutes(int reminderMinutes) {
        this.reminderMinutes = reminderMinutes;
    }

    public int getReminderHours() {
        return reminderHours;
    }

    public void setReminderHours(int reminderHours) {
        this.reminderHours = reminderHours;
    }

    public int getReminderDays() {
        return reminderDays;
    }

    public void setReminderDays(int reminderDays) {
        this.reminderDays = reminderDays;
    }

}
