package com.java_iv.group_project2.model;

import com.java_iv.group_project2.database.AddToDB;
import com.java_iv.group_project2.database.ClearDB;
import com.java_iv.group_project2.database.Config;
import com.java_iv.group_project2.database.GetFromDB;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.List;

/**
 * Mock object that simulates the functionality of a database
 */
public class WalletDB {
    private EWallet wallet;
    private Connection con;


    /**
     * Initial constructor where there is no wallet saved
     */
    public WalletDB() {
        this.wallet = null;
        this.con = getConnection(Config.username, Config.password);
    }

    /**
     * This method makes a connection to the database server.
     * 
     * @param usr : username to access database
     * @param pwd : password to access database
     * @return : Connection to the database
     * @throws SQLException
     */
    private Connection getConnection(String usr, String pwd) {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        Connection con = null;
        try {
            con = DriverManager.getConnection(url, usr, pwd);
            System.out.println("Connected to the server");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("Connection to database - FAILED\n");
        }
        return con;

    }

    /**
     * Save the wallet into the database
     *
     * @param newWallet
     */
    public void saveEWalletToDB(EWallet newWallet) {
        ClearDB.clearTables(this.con);
        List<CreditCard> credit_cards = newWallet.listOfCreditCards();
        List<DebitCard> debit_cards = newWallet.listOfDebitCards();
        List<PersonalCard> personal_cards = newWallet.listOfPersonalCards();
        List<Note> notes = newWallet.listOfNotes();

        saveCreditCardsToDB(credit_cards);
        saveDebitCardsToDB(debit_cards);
        savePersonalCardsToDB(personal_cards);
        saveNoteToDB(notes);
        AddToDB.addCash(this.con, newWallet.getCash());
    }

    /**
     * Save Credit Cards to the db
     *
     * @param credit_cards
     */
    private void saveCreditCardsToDB(List<CreditCard> credit_cards) {
        for (CreditCard creditCard : credit_cards) {
            AddToDB.addCreditCard(this.con, creditCard);
        }
    }

    /**
     * Save Debit Cards to db
     *
     * @param debit_cards
     */
    private void saveDebitCardsToDB(List<DebitCard> debit_cards) {
        for (DebitCard debitCard : debit_cards) {
            AddToDB.addDebitCard(this.con, debitCard);
        }
    }

    /**
     * Save Personal Cards to db
     *
     * @param personal_cards
     */
    private void savePersonalCardsToDB(List<PersonalCard> personal_cards) {
        for (PersonalCard personalCard : personal_cards) {
            AddToDB.addPersonalCard(this.con, personalCard);
        }
    }

    /**
     * Save Notes to db
     *
     * @param notes
     */
    private void saveNoteToDB(List<Note> notes) {
        for (Note note : notes) {
            AddToDB.addNote(this.con, note);
        }
    }


    /**
     * Load the wallet from the database
     *
     * @return
     */
    public EWallet loadEWalletFromDB() {
        this.wallet = new EWallet();
        refreshNoteId();
        try {
            addDebitCards();
            addCreditCards();
            addPersonalCards();
            addNotes();
            addCash();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new EWallet(this.wallet);
    }

    public void addCreditCards() throws Exception {
        String query = "SELECT * FROM CREDIT_CARD";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        System.out.println();
        while (rs.next()) {
            String num = rs.getString("CREDIT_CARD_NUMBER");
            String name = rs.getString("CARD_HOLDER_NAME");
            java.util.Date date = stringToDate(rs.getString("EXPIRYDATE"));
            int code = rs.getInt("SECURITY_CODE");
            String bankName = rs.getString("BANK_NAME");
            int limit = rs.getInt("LIMIT");
            this.wallet.addCard(name, num, date, code, bankName, limit);
        }
        rs.close();
        stmt.close();
    }

    public void addDebitCards() throws Exception {
        String query = "SELECT * FROM DEBIT_CARD";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            String num = rs.getString("DEBIT_CARD_NUMBER");
            String name = rs.getString("CARD_HOLDER_NAME");
            java.util.Date date = stringToDate(rs.getString("EXPIRYDATE"));
            int code = rs.getInt("SECURITY_CODE");
            String bankName = rs.getString("BANK_NAME");
            double funds = rs.getDouble("FUNDS");
            this.wallet.addCard(name, num, date, code, bankName, funds);
        }
        rs.close();
        stmt.close();
    }

    public void addPersonalCards() throws Exception {
        String query = "SELECT * FROM PERSONAL_CARD";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            String num = rs.getString("PERSONAL_CARD_NUMBER");
            String name = rs.getString("CARD_HOLDER_NAME");
            String stringDate = rs.getString("EXPIRYDATE");
            Date date = null;
            if (stringDate != null) {
                date = stringToDate(stringDate);
                this.wallet.addCard(name, num, date);
            } else {
                this.wallet.addCard(name, num);
            }

        }
        rs.close();
        stmt.close();
    }

    public void addNotes() throws Exception {
        String query = "SELECT * FROM NOTE";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            this.wallet.addNote();
            List<Note> notesList = this.wallet.listOfNotes();
            Note newNote = notesList.get(notesList.size() - 1);
            ;
            newNote.setTitle(rs.getString("TITLE"));
            newNote.setBody(rs.getString("BODY"));
            newNote.setReminderActive(rs.getInt("IS_REMINDER_ACTIVE") != 0);
            newNote.setReminderMinutes(rs.getInt("REMINDER_MINUTES"));
            newNote.setReminderHours(rs.getInt("REMINDER_HOURS"));
            newNote.setReminderDays(rs.getInt("REMINDER_DAYS"));
        }
        rs.close();
        stmt.close();
    }

    public void addCash() throws Exception {
        String query = "SELECT * FROM CASH";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            Cash cash = this.wallet.getCash();
            cash.setCash(rs.getDouble("CASH_AMOUNT"));
        }
        rs.close();
        stmt.close();
    }

    public java.util.Date stringToDate(String date) throws ParseException {
        DateFormat sourceFormat = new SimpleDateFormat("MM/yyyy");
        return sourceFormat.parse(date);
    }

    private void refreshNoteId() {
        Note.sequenceId = GetFromDB.getLastNoteId(this.con) + 1;
        System.out.println(GetFromDB.getLastNoteId(this.con) + 1);
    }
}
