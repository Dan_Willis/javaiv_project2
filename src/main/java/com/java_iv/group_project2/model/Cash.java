package com.java_iv.group_project2.model;


import com.java_iv.group_project2.view.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent the cash in the e-wallet
 */
public class Cash implements Subject {
    private double cash;
    private List<Observer> cashObservers;

    /**
     * Constructor that set the initial amount of money to 0
     */
    public Cash(){
        this.cash = 0.0;
        this.cashObservers = new ArrayList<>();
    }

    /**
     * Constructor to set an initial amount of money
     * @param amount
     */
    public Cash(double amount){
        this.cash = amount;
        this.cashObservers = new ArrayList<>();
    }

    public Cash(Cash cash){
        this.cash = cash.cash;
        this.cashObservers = new ArrayList<>();
    }


    public void setCash(double cash) {
        this.cash = cash;
        notifyObserver();
    }

    /**
     * Add cash to the e-wallet
     * 
     * @param money
     */
    public void addCash(double money) {
        this.cash += money;
        // Notify to change the view
        notifyObserver();
    }

    /**
     * Removes cash from the wallet
     * 
     * @param money
     * @return
     */
    public boolean removeCash(double money) {
        if (!(this.cash - money < 0)) {
            this.cash -= money;
            // Notify to change the view
            notifyObserver();
            return true;
        }
        return false;
    }

    /**
     * Returns the amount of cash in the e-wallet
     * 
     * @return
     */
    public double getCash() {
        return cash;
    }

    @Override
    public void register(Observer o) {
        this.cashObservers.add(o);
    }

    @Override
    public void unregister(Observer o) {
        this.cashObservers.remove(o);
    }

    @Override
    public void notifyObserver() {
        for (Observer o : cashObservers) {
            o.update(this.cash);
        }
    }
}
