package com.java_iv.group_project2.model;

import java.util.Random;

/**
 * Mock object that simulates the functionality a bank account
 */
public class DebitCardIssuer {
    private String bankName;
    private double funds;

    /**
     * Constructor for bank account linked to debit card
     * @param bankName
     * @param funds
     */
    public DebitCardIssuer(String bankName, double funds) {
        this.bankName = bankName;
        this.funds = funds;
    }

    /**
     * Withdraw an amount from the bank account
     * @param amount
     */
    public void withdraw(double amount) throws Exception{
        Random rand = new Random();
        int int_random = rand.nextInt(5);
        if(int_random == 1){
            throw new Exception("Payment Rejected. Network problem");
        }
        this.funds -= amount;
    }
    
    /**
     * Returns the balance in the bank account
     * @return
     */
    public double getBalance(){
        return this.funds;
    }

    /**
     * Returns the bank name
     * @return
     */
    public String getBankName() {
        return bankName;
    }
}
