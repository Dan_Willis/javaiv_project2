package com.java_iv.group_project2.model;
import java.util.Random;

/**
 * Mock object that simulates the functionality a credit card issuer
 */
public class CreditCardIssuer {
    private String bankName;
    private int limit;
    private double moneySpent;

    public CreditCardIssuer(String bankName, int limit) {
        this.bankName = bankName;
        this.limit = limit;
        this.moneySpent = 0;
    }

    /**
     * Check if the money spent exceeds 50% of the credit card limit
     * @return
     */
    public boolean exceed50PercentLimit(){
        double limit50Percent = this.limit / 2;
        if (this.moneySpent >= limit50Percent){
            return true;
        }
        return false;  
    }

    /**
     * Spends money with the 
     * @param amount
     * @return
     */
    public boolean spend(double amount) throws Exception{
        Random rand = new Random();
        int int_random = rand.nextInt(5);
        if(int_random == 1){
            throw new Exception("Payment Rejected. Network problem");
        }
        double moneyLeft = this.limit - this.moneySpent;
        if (moneyLeft >= amount){
            this.moneySpent += amount;
            //In the controller call exceed50PercentLimit() and if true, use setText() to say reached at least 50%
            return true;
        }
        return false;
        // In the controller if it is false, return an error to say that the card is rejected because of insufficient funds
    }

    /**
     * Returns the name of the bank
     * @return
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * Returns the limit of the card linked to the bank account
     * @return
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Returns the money spent with the credit card
     * @return
     */
    public double getMoneySpent(){
        return moneySpent;
    }


}
