package com.java_iv.group_project2.model;
import java.util.Date;

public class PersonalCard extends Card{

    /**
     * Creates a personal card that has an expiry date
     * @param cardHolderName
     * @param cardNumber
     * @param expiryDate
     */
    public PersonalCard(String cardHolderName, String cardNumber, Date date) {
        super(cardHolderName, cardNumber, date);
    } 

    /**
     * Creates a personal card that doesn't have an expiry date
     * @param cardHolderName
     * @param cardNumber
     */
    public PersonalCard(String cardHolderName, String cardNumber) {
        super(cardHolderName, cardNumber);
    }
}
