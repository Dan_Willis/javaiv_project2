package com.java_iv.group_project2.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EWallet implements IWallet {
    private List<WalletItem> items;
    private Cash cash;
    private WalletDB db;
    private boolean lastLoadSuccessful;
    private boolean lastSaveSuccessful;

    private final int MAX_NUMBER_OF_NOTES = 10;
    private final int MAX_NUMBER_OF_CARDS = 10;
    
    /**
     * Constructor that initialize the arraylist of items
     */
    public EWallet() {
        this.items = new ArrayList<>();
        this.cash = new Cash();
        this.db = new WalletDB();
    }

    public boolean isLastSaveSuccessful() {
        return lastSaveSuccessful;
    }

    /**
     * resets it to false
     */
    public void resetLastSaveSuccessful() {
        this.lastSaveSuccessful = false;
    }

    public boolean isLastLoadSuccessful() {
        return lastLoadSuccessful;
    }

    /**
     * resets it to false
     */
    public void resetLastLoadSuccessful() {
        this.lastLoadSuccessful = false;
    }

    /**
     * copy constructor
     * @param wallet
     */
    public EWallet(EWallet wallet){
        this.copyEWallet(wallet);
    }

    /**
     * saves this ewallet to the db
     */
    public void saveWallet(){
        // artificially long to text threading
        for (int j = 0; j < 10000; j++) {
            System.out.println("" + j);
        }
        db.saveEWalletToDB(this);
        this.lastSaveSuccessful = true;
    }
    
    /**
     * loads this ewallet from the db
     */
    public void loadWallet(){
        
        // artificially long to text threading
        for (int j = 0; j < 100000; j++) {
            System.out.println("" + j);
        }
        
        EWallet newEWallet = db.loadEWalletFromDB();
        if(newEWallet == null){
            this.lastLoadSuccessful = false;
            return;
        } else {
            this.copyEWallet(newEWallet);
            this.lastLoadSuccessful = true;
        }
    }

    /**
     * copies the fields of the given ewallet to this ewallet
     * @param wallet
     */
    private void copyEWallet(EWallet wallet){
        this.items = new ArrayList<WalletItem>(wallet.items);
        this.cash = new Cash(wallet.cash);
        this.db = wallet.db;
    }

    /**
     * Add a debit card to the e-wallet
     * 
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @param securityCode
     * @param bankName
     * @param funds
     * @return
     */
    public boolean addCard(String cardHolderName, String cardNumber, Date date, int securityCode, String bankName,
            double funds) throws Exception {
        if (checkIfDebitCardExists(cardNumber)){
            throw new Exception("You already have a debit card with this card number");
        }
        WalletItem item = createDebitCard(cardHolderName, cardNumber, date, securityCode, bankName, funds);
        if (item instanceof Card) {
            if (checkNumberOfCards(this.items) < MAX_NUMBER_OF_CARDS) {
                items.add(item);
                return true;
            }
        }
        return false;
    }

    /**
     * Add a credit card to the e-wallet
     * 
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @param securityCode
     * @param bankName
     * @param limit
     * @return
     */
    public boolean addCard(String cardHolderName, String cardNumber, Date date, int securityCode, String bankName, int limit) throws Exception {
        if (checkIfCreditCardExists(cardNumber)){
            throw new Exception("You already have a credit card with this card number");
        }
        WalletItem item = createCreditCard(cardHolderName, cardNumber, date, securityCode, bankName, limit);
        if (item instanceof Card) {
            if (checkNumberOfCards(this.items) < MAX_NUMBER_OF_NOTES) {
                items.add(item);
                return true;
            }
        }
        return false;
    }

    /**
     * Add a personal card with date to the e-wallet
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @return
     */
    public boolean addCard(String cardHolderName, String cardNumber, Date date) throws Exception {
        if (checkIfPersonalCardExists(cardNumber)){
            throw new Exception("You already have a personal card with this card number");
        }
        WalletItem item = createPersonalCard(cardHolderName, cardNumber, date);
        if (item instanceof Card) {
            if (checkNumberOfCards(this.items) < MAX_NUMBER_OF_CARDS) {
                items.add(item);
                return true;
            }
        }
        return false;
    }

    /**
     * Add a personal card without date to the e-wallet
     * @param cardHolderName
     * @param cardNumber
     * @return
     */
    public boolean addCard(String cardHolderName, String cardNumber) throws Exception{
        if (checkIfPersonalCardExists(cardNumber)){
            throw new Exception("You already have a personal card with this card number");
        }
        WalletItem item = createPersonalCard(cardHolderName, cardNumber);
        if (item instanceof Card) {
            if (checkNumberOfCards(this.items) < MAX_NUMBER_OF_CARDS) {
                items.add(item);
                return true;
            }
        }
        return false;
    }

    /**
     * Add a note to the e-wallet
     * return the id of the new note
     * @return
     */
    public int addNote() {
        Note item = createNote();

        if (checkNumberOfNotes(this.items) < MAX_NUMBER_OF_NOTES) {
            items.add(item);
            return item.getNoteId();
        }

        return -1;
    }

    /**
     * Returns a new Debit Card
     * 
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @param securityCode
     * @param bankName
     * @param funds
     * @return
     */
    private WalletItem createDebitCard(String cardHolderName, String cardNumber, Date date, int securityCode,
            String bankName, double funds) {
        return new DebitCard(cardHolderName, cardNumber, date, securityCode, bankName, funds);
    }

    /**
     * Returns a new CreditCard
     * 
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @param securityCode
     * @param bankName
     * @param limit
     * @return
     */
    private WalletItem createCreditCard(String cardHolderName, String cardNumber, Date date, int securityCode,
            String bankName, int limit) {
        return new CreditCard(cardHolderName, cardNumber, date, securityCode, bankName, limit);
    }

    /**
     * Returns a personal card with date
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @return
     */
    private WalletItem createPersonalCard(String cardHolderName, String cardNumber, Date date) {
        return new PersonalCard(cardHolderName, cardNumber, date);
    }

    /**
     * Returns a personal card without a date
     * @param cardHolderName
     * @param cardNumber
     * @return
     */
    private WalletItem createPersonalCard(String cardHolderName, String cardNumber) {
        return new PersonalCard(cardHolderName, cardNumber);
    }

    /**
     * Returns a new note
     * 
     * @param body
     * @param issueReminder
     * @return
     */
    private Note createNote() {
        return new Note();
    }

    /**
     * Returns the number of cards in an arraylist of walletItems
     * 
     * @param items
     * @return
     */
    private static int checkNumberOfCards(List<WalletItem> items) {
        int count = 0;
        for (WalletItem walletItem : items) {
            if (walletItem instanceof Card) {
                count++;
            }
        }
        return count;
    }

    /**
     * check if a credit card already exists based on the card number
     * @param cardNumber
     * @return
     */
    private boolean checkIfCreditCardExists(String cardNumber){
        for (CreditCard card : listOfCreditCards()) {
            if (card.getCardNumber().equals(cardNumber)){
                return true;
            }
        }
        return false;
    }

    /**
     * check if a debit card already exists based on the card number
     * @param cardNumber
     * @return
     */
    private boolean checkIfDebitCardExists(String cardNumber){
        for (DebitCard card : listOfDebitCards()) {
            if (card.getCardNumber().equals(cardNumber)){
                return true;
            }
        }
        return false;
    }

    /**
     * check if a personal card already exists based on the card number
     * @param cardNumber
     * @return
     */
    private boolean checkIfPersonalCardExists(String cardNumber){
        for (PersonalCard card : listOfPersonalCards()) {
            if (card.getCardNumber().equals(cardNumber)){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the number of notes in an arraylist of walletItems
     * 
     * @param items
     * @return
     */
    private static int checkNumberOfNotes(List<WalletItem> items) {
        int count = 0;
        for (WalletItem walletItem : items) {
            if (walletItem instanceof Note) {
                count++;
            }
        }
        return count;
    }

    /**
     * Returns a list of credit card
     * 
     * @return
     */
    public List<CreditCard> listOfCreditCards(){
        List<CreditCard> list = new ArrayList<>();
        for (WalletItem walletItem : this.items) {
            if (walletItem instanceof CreditCard) {
                list.add((CreditCard) walletItem);
            }
        }
        return list;
    }

    /**
     * Returns a list of debit card
     * 
     * @return
     */
    public List<DebitCard> listOfDebitCards(){
        List<DebitCard> list = new ArrayList<>();
        for (WalletItem walletItem : this.items) {
            if (walletItem instanceof DebitCard) {
                list.add((DebitCard) walletItem);
            }
        }
        return list;
    }

    /**
     * Returns a list of personal card
     * @return
     */
    public List<PersonalCard> listOfPersonalCards(){
        List<PersonalCard> list = new ArrayList<>();
        for (WalletItem walletItem : this.items) {
            if (walletItem instanceof PersonalCard){
                list.add((PersonalCard)walletItem);
            }
        }
        return list;
    }

    /**
     * Returns list of notes
     * 
     * @return
     */
    public List<Note> listOfNotes(){
        List<Note> list = new ArrayList<>();
        for (WalletItem walletItem : this.items) {
            if (walletItem instanceof Note) {
                list.add((Note) walletItem);
            }
        }
        return list;
    }

    /**
     * Removes a card in the e-wallet
     * 
     * @param cardNumber
     */
    public void removeCard(String cardNumber) {
        for (WalletItem walletItem : this.items) {
            if (walletItem instanceof Card) {
                if (((Card) walletItem).getCardNumber() == cardNumber) {
                    this.items.remove(walletItem);
                    return;
                }
            }
        }
    }

    /**
     * Gets a card in the e-wallet based on card Number
     *
     * @param cardNumber
     */
    public Card getCard(String cardNumber) {
        for (WalletItem walletItem : this.items) {
            if (walletItem instanceof Card) {
                if (((Card) walletItem).getCardNumber() == cardNumber) {
                    return (Card) walletItem;
                }
            }
        } return null;
    }

    /**
     * Removes a note in the e-wallet
     * 
     * @param noteId
     */
    public void removeNote(int noteId) {
        List<Note> list = listOfNotes();
        for (Note note : list) {
            
            if (note.getNoteId() == noteId) {
                this.items.remove(note);
            }

        }
    }

    /**
     * Updates a specific note that has no reminder set
     * @param noteId
     * @param title
     * @param body
     */
    public void updateNote(int noteId, String title, String body) {
        Note note = findNote(noteId);

        note.setTitle(title);
        note.setBody(body);
    }

    /**
     * Updates a specific note that has a reminder set
     * @param noteId
     * @param title
     * @param body
     * @param reminderMinutes
     * @param reminderHours
     * @param reminderDays
     */
    public void updateNote(int noteId, String title, String body, int reminderMinutes, int reminderHours, int reminderDays) {
        Note note = findNote(noteId);

        note.setTitle(title);
        note.setBody(body);
        note.setReminderMinutes(reminderMinutes);
        note.setReminderHours(reminderHours);
        note.setReminderDays(reminderDays);
        
    }

    /**
     * Returns a specific according to his id
     * @param noteId
     * @return
     */
    public Note findNote(int noteId) {
        for (WalletItem walletItem : this.items) {
            if (walletItem instanceof Note) {
                if (((Note) walletItem).getNoteId() == noteId) {
                    return (Note) walletItem;
                }
            }
        }

        return null;
    }

    /**
     * Returns the number of items in the EWallet
     * @return
     */
    public int getSize(){
        return items.size();
    }

    public Cash getCash() {
        return cash;
    }
}
