package com.java_iv.group_project2.model;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents a general card object
 */
public abstract class Card implements WalletItem{
    protected String cardHolderName;
    protected String cardNumber;
    protected String expiryDate;

    /**
     * Creates a card that has an expiry date
     * @param cardHolderName
     * @param cardNumber
     * @param date
     */
    public Card(String cardHolderName, String cardNumber, Date date) {
        this.cardHolderName = cardHolderName;
        this.cardNumber = cardNumber;
        this.expiryDate = new SimpleDateFormat("MM/yyyy").format(date);
    }

    /**
     * Creates a card that doesn't have an expiry date
     * @param cardHolderName
     * @param cardNumber
     */
    public Card(String cardHolderName, String cardNumber) {
        this.cardHolderName = cardHolderName;
        this.cardNumber = cardNumber;
    }

    /**
     * Returns the card holder name
     * @return
     */
    public String getCardHolderName() {
        return cardHolderName;
    }

    /**
     * Returns the card number
     * @return
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Returns the expiry date
     * @return
     */
    public String getExpiryDate() {
        return expiryDate;
    }
}
