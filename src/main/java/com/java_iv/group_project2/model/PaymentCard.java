package com.java_iv.group_project2.model;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * Abstract class that represent the payment cards
 */
public abstract class PaymentCard extends Card{
    protected int securityCode;

    /**
     * Constructor that set the general information of a payment card
     * @param cardHolderName
     * @param cardNumber
     * @param date
     * @param securityCode
     */
    public PaymentCard(String cardHolderName, String cardNumber, Date date, int securityCode) {
        super(cardHolderName, cardNumber, date);
        this.securityCode = securityCode;
    }

    /**
     * Returns the security code
     * @return
     */
    public int getSecurityCode() {
        return securityCode;
    }
}
