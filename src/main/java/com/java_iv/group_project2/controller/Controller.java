package com.java_iv.group_project2.controller;

import java.util.ArrayList;
import java.util.List;

import com.java_iv.group_project2.controller.PaymentStrategy.Payment;
import com.java_iv.group_project2.model.*;
import com.java_iv.group_project2.view.CardUI;
import com.java_iv.group_project2.view.CardView;
import com.java_iv.group_project2.view.MainView;
import com.java_iv.group_project2.view.PaymentCardUI;
import com.java_iv.group_project2.view.notesView.NoteView;
import com.java_iv.group_project2.view.notesView.ReminderForm;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.application.Platform;

/**
 * Represents the controller of the app
 */
public class Controller {
    private MainView view;
    private EWallet model;
    private Payment payment;

    /**
     * sets the view and the model that the controller will manage
     * @param view
     * @param model
     */
    public void setViewModel(MainView view, EWallet model, Payment payment) {
        this.view = view;
        this.model = model;
        this.payment = payment;
        this.view.setAddCardBtnListener(new AddCardEvent(model, view));
        this.view.setInfoPaneDeleteBtnListener(new RemoveCardEvent(model, view));
        this.view.setInfoPanePayBtnListener(new CardPayEvent(model, view, this.payment));
        this.view.setCashPaneAddBtnListener(new CashAddEvent(model, view));
        this.view.setCashPanePayBtnListener(new CashPayEvent(model, view, this.payment));
    }

    /**
     * updates a note in the model
     * @param id
     * @param title
     * @param body
     */
    public void updateNote(int id, String title, String body) {
        this.model.updateNote(id, title, body);
    }
    /**
     * updates a note in the moddel
     * @param id
     * @param title
     * @param body
     * @param reminderMinutes
     * @param reminderHours
     * @param reminderDays
     */
    public void updateNote(int id, String title, String body, int reminderMinutes, int reminderHours, int reminderDays) {
        this.model.updateNote(id, title, body, reminderMinutes, reminderHours, reminderDays);
    }

    /**
     * gets the title of a note given its id
     * @param id
     * @return
     */
    public String getNoteTitle(int id) {
        return this.model.findNote(id).getTitle();
    }

    /**
     * creates an empty note
     * @return
     */
    public int createNote() {
        return this.model.addNote();
    }

    /**
     * adds the note info from the model to the view
     * @param id
     * @param tfTitle
     * @param taBody
     * @param rForm
     */
    public void viewNote(int id, TextField tfTitle, TextArea taBody, ReminderForm rForm) {
        Note note = this.model.findNote(id);
        tfTitle.setText(note.getTitle());
        taBody.setText(note.getBody());
        rForm.setMinutes(note.getReminderMinutes());
        rForm.setHours(note.getReminderHours());
        rForm.setDays(note.getReminderDays());
        rForm.syncReminderActiveFrequencyValid();
    }

    /**
     * deletes a note given its id
     * @param id
     */
    public void deleteNote(int id) {
        this.model.removeNote(id);
    }

    /**
     * saves the wallet to the database
     */
    public void saveEWalletToDB(){
        view.getMessageText().setText("Saving...");
        view.setSaveLoadDisabled(true);
        view.playWaitingAnim();
        Thread t = new Thread(new Runnable() {
            public void run(){
                model.saveWallet();

                Platform.runLater(
                    () -> {
                        if(model.isLastSaveSuccessful()){
                            view.getMessageText().setText("Saved wallet successfully!");
                            model.resetLastSaveSuccessful();
                        } 
                        view.setSaveLoadDisabled(false);  
                        view.stopWaitingAnim();             
                    }
                );
            }
        }); 
        t.start();
    }

    /**
     * loads the wallet from the database
     */
    public void loadEWalletFromDB() {
        // empty load add

        // Reset view
        NoteView noteView = view.getNoteView();
        noteView.empty();
        CardView cardView = view.getCardView();
        cardView.empty();
        this.view.getCardView().getCashPane().setMessage("");
        this.view.getCardView().getCashPane().getPaymentField().setText("");

        view.getMessageText().setText("Loading...");
        view.setViewsDisabled(true);
        view.setSaveLoadDisabled(true);
        view.playWaitingAnim();
        Thread t = new Thread(new Runnable() {
            public void run(){
                model.loadWallet();

                Platform.runLater(() -> {
                    updateViewAfterLoad(noteView);
                    view.stopWaitingAnim();
                });
            }
        });
        t.start();       

    }

    /**
     * updates the view after the wallet has been loaded
     */
    private void updateViewAfterLoad(NoteView noteView) {
        if(!model.isLastLoadSuccessful()){
            // show to the user that cant load because nothing was saved
            // make a text object in the main view to show (something is loading o, saved succesfull, loaded successfully, unable to lead)
            // or maybe the feature is that when nothing is saved: what is loaded is an empty wallet
            view.getMessageText().setText("Loaded an empty wallet");
            model.resetLastLoadSuccessful();
            view.setViewsDisabled(false);
            view.setSaveLoadDisabled(false);
            return;
        }
        List<CreditCard> creditCards = model.listOfCreditCards();
        List<DebitCard> debitCards = model.listOfDebitCards();
        List<PersonalCard> personalCards = model.listOfPersonalCards();
        List<Note> notes = model.listOfNotes();

        for (Note note : notes) {
            noteView.addNote(note.getNoteId());
        }

        // Loading Cards

        // Debit
        for (CardUI cardUI : convertCards(debitCards)) {
            this.view.addDebitCard((PaymentCardUI) cardUI);
        }

        // Credit
        for (CardUI cardUI : convertCards(creditCards)) {
            this.view.addCreditCard((PaymentCardUI) cardUI);
        }

        // Personal
        for (CardUI cardUI : convertCards(personalCards)) {
            this.view.addPersonalCard(cardUI);
        }

        // Cash
        Cash cash = this.model.getCash();
        cash.register(this.view.getCardView().getCashPane());
        cash.notifyObserver();
        
        view.setViewsDisabled(false);
        view.setSaveLoadDisabled(false);
        view.getMessageText().setText("Loaded wallet successfully");
    }

    /**
     * Converts list of cards from DB to a list of CardUI
     * @param cards
     * @return
     */
    public List<CardUI> convertCards(List<? extends Card> cards) {
        List<CardUI> returnedList = new ArrayList<>();

        String cardHolderName = "";
        String cardNumber = "";
        String securityCode = "";
        String expiryDate = "";
        String bankName = "";
        String funds = "";

        for (Card card : cards) {
            resetFields(cardHolderName,
                    cardNumber,
                    expiryDate,
                    securityCode,
                    bankName,
                    funds);
            if (card instanceof DebitCard) {
                DebitCard debitCard = (DebitCard) card;
                cardHolderName = debitCard.getCardHolderName();
                cardNumber = debitCard.getCardNumber();
                expiryDate = debitCard.getExpiryDate();
                securityCode = String.valueOf(debitCard.getSecurityCode());
                bankName = debitCard.getBankName();
                funds = String.format("%,.2f", debitCard.getFunds());
                PaymentCardUI debitCardUI = new PaymentCardUI(cardHolderName,
                        cardNumber,
                        expiryDate,
                        securityCode,
                        bankName,
                        funds);
                returnedList.add(debitCardUI);
            } else if (card instanceof CreditCard) {
                CreditCard creditCard = (CreditCard) card;
                cardHolderName = creditCard.getCardHolderName();
                cardNumber = creditCard.getCardNumber();
                expiryDate = creditCard.getExpiryDate();
                securityCode = String.valueOf(creditCard.getSecurityCode());
                bankName = creditCard.getBankName();
                funds = String.format("%,.2f", (double) creditCard.getLimit());
                PaymentCardUI creditCardUI = new PaymentCardUI(cardHolderName,
                        cardNumber,
                        expiryDate,
                        securityCode,
                        bankName,
                        funds);
                returnedList.add(creditCardUI);
            } else {
                cardHolderName = card.getCardHolderName();
                cardNumber = card.getCardNumber();
                expiryDate = card.getExpiryDate();
                CardUI personalCardUI = new CardUI(cardHolderName, cardNumber, expiryDate);
                returnedList.add(personalCardUI);
            }
        }
        return returnedList;
    }

    /**
     * Helper method for the convertCard method: Resets the card fields to an empty string
     * @param cardHolderName
     * @param cardNumber
     * @param expiryDate
     * @param securityCode
     * @param bankName
     * @param funds
     */
    public void resetFields(String cardHolderName,
                            String cardNumber,
                            String expiryDate,
                            String securityCode,
                            String bankName,
                            String funds) {
        cardHolderName = "";
        cardNumber = "";
        expiryDate = "";
        securityCode = "";
        bankName = "";
        funds = "";
    }
}