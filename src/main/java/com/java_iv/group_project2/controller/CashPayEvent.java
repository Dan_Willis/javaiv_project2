package com.java_iv.group_project2.controller;

import com.java_iv.group_project2.controller.PaymentStrategy.CashChoice;
import com.java_iv.group_project2.controller.PaymentStrategy.Payment;
import com.java_iv.group_project2.model.Cash;
import com.java_iv.group_project2.model.EWallet;
import com.java_iv.group_project2.view.MainView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import com.java_iv.group_project2.model.EWallet;

public class CashPayEvent implements EventHandler<ActionEvent> {
    private EWallet model;
    private MainView view;
    private Payment payment;

    public CashPayEvent(EWallet model, MainView view, Payment payment) {
        this.model = model;
        this.view = view;
        this.payment = payment;
    }

    /**
     * Makes a payment from the wallet based on user input in the cash field
     */
    @Override
    public void handle(ActionEvent event) {

        try {
            double amount = Double.parseDouble(this.view.getCardView().getCashPane().getPaymentField().getText());
           this.payment.setPaymentMethod(new CashChoice());
           this.payment.makePayment(amount, this.model, this.view);
        } catch (NumberFormatException e) {
            this.view.getCardView().getCashPane().setMessage("Please enter an amount");
        }
    }
}
