package com.java_iv.group_project2.controller;

import com.java_iv.group_project2.view.AddCardPane;
import com.java_iv.group_project2.view.CardUI;
import com.java_iv.group_project2.view.MainView;
import com.java_iv.group_project2.view.PaymentCardUI;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import com.java_iv.group_project2.model.EWallet;

import java.util.Calendar;
import java.util.Date;

public class AddCardEvent implements EventHandler<ActionEvent> {
    private EWallet model;
    private MainView view;
    private AddCardPane addCardPane;
    private String cardHolderName;
    private String cardNumber;
    private int expiryMonth;
    private int expiryYear;
    private int securityCode;
    private Date expiryDate;
    private String bankName;

    public AddCardEvent(EWallet model, MainView view) {
        this.model = model;
        this.view = view;
    }

    /**
     * Adds a new card based on selected type (Debit, Credit, or personal)
     */
    @Override
    public void handle(ActionEvent event) {
        String selectedDeck = view.getCardView().getDeckToAdd();
        try {
            switch (selectedDeck) {
                case "Debit Cards":
                    addDebitCard();
                    break;
                case "Credit Cards":
                    addCreditCard();
                    break;
                case "Personal Cards":
                    addPersonalCard();
                    break;
            }
        } catch (Exception e) {
            view.getCardView().getAddCardPane().setWarningMessage("Please fill in all required fields");
        }
    }

    /**
     * Adds a new debit card to the  model and view
     */
    private void addDebitCard() {
        try {
            if (!isPaymentFieldComplete()) {
                view.getCardView().getAddCardPane().setWarningMessage("Please fill in all required fields");
            } else if (!isValidDate()) {
                view.getCardView().getAddCardPane().setWarningMessage("Please enter a valid month (1-12) and a valid year (" + Calendar.getInstance().get(Calendar.YEAR) + "-" + (Calendar.getInstance().get(Calendar.YEAR) + 50) + ").");
            } else {
                setPaymentFieldValues();
                double funds = Double.parseDouble(addCardPane.getFundsField().getText());
                try {
                    if (model.addCard(cardHolderName, cardNumber, expiryDate, securityCode, bankName, funds)) {
                        PaymentCardUI debit = new PaymentCardUI(cardHolderName,
                                cardNumber,
                                expiryMonth + "/" + expiryYear,
                                String.valueOf(securityCode),
                                bankName,
                                String.format("%,.2f", funds));
                        view.addDebitCard(debit);
                        view.getCardView().hideAddCardPane();
                    } else {
                        view.getCardView().getAddCardPane().setWarningMessage("Rejected. You can only have a maximum of 10 cards.");
                    }
                } catch (Exception e) {
                    view.getCardView().getAddCardPane().setWarningMessage(e.getMessage());
                }
            }
        } catch (NumberFormatException e) {
            view.getCardView().getAddCardPane().setWarningMessage("Please fill in all required fields");
        }
    }

    /**
     * Adds a new credit card to the model and view
     */
    private void addCreditCard() {
        try {
            if (!isPaymentFieldComplete()) {
                view.getCardView().getAddCardPane().setWarningMessage("Please fill in all required fields");
            } else if (!isValidDate()) {
                view.getCardView().getAddCardPane().setWarningMessage("Please enter a valid month (1-12) and a valid year (" + Calendar.getInstance().get(Calendar.YEAR) + "-" + (Calendar.getInstance().get(Calendar.YEAR) + 50) + ").");
            } else {
                setPaymentFieldValues();
                int funds = Integer.parseInt(addCardPane.getFundsField().getText());
                try {
                    if (model.addCard(cardHolderName, cardNumber, expiryDate, securityCode, bankName, funds)) {
                        PaymentCardUI debit = new PaymentCardUI(cardHolderName,
                                cardNumber,
                                expiryMonth + "/" + expiryYear,
                                String.valueOf(securityCode),
                                bankName,
                                String.format("%,.2f", (double) funds));
                        view.addCreditCard(debit);
                        view.getCardView().hideAddCardPane();
                    } else {
                        view.getCardView().getAddCardPane().setWarningMessage("Rejected. You can only have a maximum of 10 cards.");
                    }
                } catch (Exception e) {
                    view.getCardView().getAddCardPane().setWarningMessage(e.getMessage());
                }
            }
        } catch (NumberFormatException e) {
            view.getCardView().getAddCardPane().setWarningMessage("Please fill in all required fields");
        }
    }

    /**
     * Adds a new personal card to the model and view
     */
    private void addPersonalCard() {
        try {
            if (!isFieldComplete()) {
                view.getCardView().getAddCardPane().setWarningMessage("Please fill in all required fields");
            } else {
                setFieldValues();
                if (expiryMonth != 0 && expiryYear != 0) {
                    if (!isValidDate()) {
                        view.getCardView().getAddCardPane().setWarningMessage("Please enter a valid month (1-12) and a valid year (" + Calendar.getInstance().get(Calendar.YEAR) + "-" + (Calendar.getInstance().get(Calendar.YEAR) + 50) + ").");
                    } else {
                        try {
                            if (model.addCard(cardHolderName, cardNumber, expiryDate)) {
                                CardUI personal = new CardUI(cardHolderName, cardNumber, expiryMonth + "/" + expiryYear);
                                view.addPersonalCard(personal);
                                view.getCardView().hideAddCardPane();
                            } else {
                                view.getCardView().getAddCardPane().setWarningMessage("Rejected. You can only have a maximum of 10 cards.");
                            }
                        } catch (Exception e) {
                            view.getCardView().getAddCardPane().setWarningMessage(e.getMessage());
                        }
                    }
                } else
                    try {
                        if (model.addCard(cardHolderName, cardNumber)) {
                            CardUI personal = new CardUI(cardHolderName, cardNumber);
                            view.addPersonalCard(personal);
                            view.getCardView().hideAddCardPane();
                        } else {
                            view.getCardView().getAddCardPane().setWarningMessage("Rejected. You can only have a maximum of 10 cards.");
                        }
                    } catch (Exception e) {
                        view.getCardView().getAddCardPane().setWarningMessage(e.getMessage());
                    }
            }
        } catch (NumberFormatException e) {
            view.getCardView().getAddCardPane().setWarningMessage("Please fill in all required fields.\n" +
                    "Leave the expiry date empty if the card does not have one.");
        }
    }

    /**
     * Sets the values for personal card details based on user input
     */
    private void setFieldValues() {
        this.addCardPane = view.getCardView().getAddCardPane();
        this.cardHolderName = addCardPane.getCardHolderField().getText();
        this.cardNumber = addCardPane.getCardNumberField().getText();
        String expiryMonthText = addCardPane.getExpiryMonthField().getText();
        String expiryYearText = addCardPane.getExpiryYearField().getText();
        if (expiryMonthText.equals("") && expiryYearText.equals("")) {
            expiryMonth = 0;
            expiryYear = 0;
        } else {
            expiryMonth = Integer.parseInt(expiryMonthText);
            expiryYear = Integer.parseInt(expiryYearText);
        }


        this.expiryDate = createExpiryDate(expiryMonth, expiryYear);

        this.securityCode = 0;
        this.bankName = "";
    }


    /**
     * Sets the values for payment card details based on user input
     */
    private void setPaymentFieldValues() {
        this.addCardPane = view.getCardView().getAddCardPane();
        this.cardHolderName = addCardPane.getCardHolderField().getText();
        this.cardNumber = addCardPane.getCardNumberField().getText();
        this.expiryMonth = Integer.parseInt(addCardPane.getExpiryMonthField().getText());
        this.expiryYear = Integer.parseInt(addCardPane.getExpiryYearField().getText());
        this.securityCode = Integer.parseInt(addCardPane.getSecurityCodeField().getText());
        this.expiryDate = createExpiryDate(expiryMonth, expiryYear);
        this.bankName = addCardPane.getBankNameField().getText();
    }

    /**
     * Verifies if all fields have values
     */
    private boolean isPaymentFieldComplete() {
        this.addCardPane = view.getCardView().getAddCardPane();
        return !addCardPane.getCardHolderField().getText().equals("") &&
                !addCardPane.getCardNumberField().getText().equals("") &&
                !addCardPane.getExpiryMonthField().getText().equals("") &&
                !addCardPane.getExpiryYearField().getText().equals("") &&
                !addCardPane.getSecurityCodeField().getText().equals("") &&
                !addCardPane.getBankNameField().getText().equals("");
    }

    private boolean isFieldComplete() {
        this.addCardPane = view.getCardView().getAddCardPane();
        return !addCardPane.getCardHolderField().getText().equals("") && !addCardPane.getCardNumberField().getText().equals("");
    }

    /**
     * Checks if the date is valid
     */
    private boolean isValidDate() {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int month = Integer.parseInt(addCardPane.getExpiryMonthField().getText());
        int year = Integer.parseInt(addCardPane.getExpiryYearField().getText());
        return month > 0 && month <= 12 && year >= currentYear && year <= (currentYear + 50);
    }

    /**
     * Creates a date object based on the number of month and year
     */
    private Date createExpiryDate(int month, int year) {
        Calendar calendar = Calendar.getInstance();
        month--;
        calendar.clear();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }
}
