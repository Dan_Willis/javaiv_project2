package com.java_iv.group_project2.controller.PaymentStrategy;

import com.java_iv.group_project2.model.EWallet;
import com.java_iv.group_project2.view.MainView;

public class Payment {
    private IPayment paymentMethod;

    public void makePayment(double amount, EWallet model, MainView view){
        this.paymentMethod.makePayment(amount, model, view);
    }

    public void setPaymentMethod(IPayment paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
