package com.java_iv.group_project2.controller.PaymentStrategy;

import com.java_iv.group_project2.model.Card;
import com.java_iv.group_project2.model.DebitCard;
import com.java_iv.group_project2.model.EWallet;
import com.java_iv.group_project2.view.CardUI;
import com.java_iv.group_project2.view.MainView;
import com.java_iv.group_project2.view.PaymentCardUI;

public class DebitChoice implements IPayment{
    @Override
    public void makePayment(double amount, EWallet model, MainView view) {
        CardUI card = view.getCardView().getCardPane().getCurrentSelectedCard();
        String cardNum = card.toString();
        Card paymentCard = model.getCard(cardNum);
        DebitCard debitCard = (DebitCard) paymentCard;
        try {
            if (debitCard.withdraw(amount)) {
                double fundsRemaining = debitCard.getFunds();
                PaymentCardUI paymentCardFX = (PaymentCardUI) card;
                paymentCardFX.setFunds(fundsRemaining);
                view.getCardView().showPaymentCardDetails(paymentCardFX, "Funds: $");
                view.getCardView().getCardInfo().setWarningText("Payment successful");
            } else {
                view.getCardView().getCardInfo().setWarningText("Payment Rejected. You have insufficient funds.");
            }
        } catch (Exception e) {
            view.getCardView().getCardInfo().setWarningText(e.getMessage());
        }
    }
}
