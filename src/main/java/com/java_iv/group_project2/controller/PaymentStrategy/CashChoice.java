package com.java_iv.group_project2.controller.PaymentStrategy;

import com.java_iv.group_project2.model.Cash;
import com.java_iv.group_project2.model.EWallet;
import com.java_iv.group_project2.view.MainView;

public class CashChoice implements IPayment{
    @Override
    public void makePayment(double amount, EWallet model, MainView view) {
            Cash cash = model.getCash();
            cash.register(view.getCardView().getCashPane());
            if (cash.removeCash(amount)) {
                view.getCardView().getCashPane().setMessage("Payment successful!");
            } else {
                view.getCardView().getCashPane().setMessage("Insufficient funds!");
            }
    }
}
