package com.java_iv.group_project2.controller.PaymentStrategy;

import com.java_iv.group_project2.model.EWallet;
import com.java_iv.group_project2.view.MainView;

public interface IPayment {
    void makePayment(double amount, EWallet model, MainView view);
}
