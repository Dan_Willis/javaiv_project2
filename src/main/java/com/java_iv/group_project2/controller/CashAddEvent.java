package com.java_iv.group_project2.controller;

import com.java_iv.group_project2.view.MainView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import com.java_iv.group_project2.model.EWallet;
import com.java_iv.group_project2.model.Cash;

public class CashAddEvent implements EventHandler<ActionEvent> {
    private EWallet model;
    private MainView view;

    public CashAddEvent(com.java_iv.group_project2.model.EWallet model, MainView view) {
        this.model = model;
        this.view = view;
    }

    /**
     * Adds cash to wallet based on user input in the cash field
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            double amount = Double.parseDouble(this.view.getCardView().getCashPane().getPaymentField().getText());
            Cash cash = this.model.getCash();
            cash.register(this.view.getCardView().getCashPane());
            cash.addCash(amount);
            this.view.getCardView().getCashPane().setMessage("Cash successfully added!");
        } catch (NumberFormatException e) {
            this.view.getCardView().getCashPane().setMessage("Please enter an amount");
        }
    }
}
