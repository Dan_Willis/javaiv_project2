package com.java_iv.group_project2.controller;

import com.java_iv.group_project2.controller.PaymentStrategy.CreditChoice;
import com.java_iv.group_project2.controller.PaymentStrategy.DebitChoice;
import com.java_iv.group_project2.controller.PaymentStrategy.Payment;
import com.java_iv.group_project2.view.CardUI;
import com.java_iv.group_project2.view.MainView;
import com.java_iv.group_project2.view.PaymentCardUI;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import com.java_iv.group_project2.model.EWallet;
import com.java_iv.group_project2.model.DebitCard;
import com.java_iv.group_project2.model.Card;
import com.java_iv.group_project2.model.CreditCard;

public class CardPayEvent implements EventHandler<ActionEvent> {
    private EWallet model;
    private MainView view;
    private Payment payment;

    public CardPayEvent(EWallet model, MainView view, Payment payment) {
        this.model = model;
        this.view = view;
        this.payment = payment;

    }

    /**
     * Makes payment based on user input and the currently selected card
     */
    @Override
    public void handle(ActionEvent event) {
        try {
            CardUI card = this.view.getCardView().getCardPane().getCurrentSelectedCard();
            String deck = card.getParent().getParent().getParent().getParent().getParent().toString();
            double amount = Double.parseDouble(this.view.getCardView().getCardInfo().getPayField().getText());
            switch (deck) {
                case "Debit Cards":
                    this.payment.setPaymentMethod(new DebitChoice());
                    break;
                case "Credit Cards":
                    this.payment.setPaymentMethod(new CreditChoice());
                    break;
            }
            this.payment.makePayment(amount, model, view);
        } catch (NumberFormatException e) {
            this.view.getCardView().getCardInfo().setWarningText("Please enter an amount.");
        }

    }


}
