package com.java_iv.group_project2.controller;

import com.java_iv.group_project2.view.CardUI;
import com.java_iv.group_project2.view.MainView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import com.java_iv.group_project2.model.EWallet;
import javafx.scene.layout.VBox;

public class RemoveCardEvent implements EventHandler<ActionEvent> {
    EWallet model;
    MainView view;

    public RemoveCardEvent(EWallet model, MainView view) {
        this.model = model;
        this.view = view;
    }

    /**
     * Removes a card from the database and the UI based on the currently selected card in the card pane
     */
    @Override
    public void handle(ActionEvent event) {
        CardUI cardToDelete = this.view.getCardView().getCardPane().getCurrentSelectedCard();
        VBox deck = (VBox) cardToDelete.getParent();
        deck.getChildren().remove(cardToDelete);
        this.view.getCardView().getCardPane().setCurrentSelectedCard(null);
        this.view.getCardView().resetCardDetails();
        this.model.removeCard(String.valueOf(cardToDelete));
        this.view.requestFocus();
    }
}
