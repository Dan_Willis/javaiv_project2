package com.java_iv.group_project2.database;

import com.java_iv.group_project2.model.DebitCard;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ClearDB {

    public static void clearTables(Connection con) {
       clearCreditTable(con);
       clearDebitTable(con);
       clearPersonalTable(con);
       clearCashTable(con);
       clearNoteTable(con);
    }

    private static void clearCreditTable(Connection con) {
        String query = "DELETE FROM CREDIT_CARD";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void clearDebitTable(Connection con) {
        String query = "DELETE FROM DEBIT_CARD";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void clearPersonalTable(Connection con) {
        String query = "DELETE FROM PERSONAL_CARD";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void clearCashTable(Connection con) {
        String query = "DELETE FROM CASH";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void clearNoteTable(Connection con) {
        String query = "DELETE FROM NOTE";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
