package com.java_iv.group_project2.database;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.java_iv.group_project2.model.Cash;
import com.java_iv.group_project2.model.CreditCard;
import com.java_iv.group_project2.model.DebitCard;
import com.java_iv.group_project2.model.Note;
import com.java_iv.group_project2.model.PersonalCard;

public class AddToDB {
    /**
     * This method adds a credit card in the database
     */
    public static void addCreditCard(Connection con, CreditCard card) {
        
        String query = "insert into credit_card values(?,?,?,?,?,?)";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, card.getCardNumber());
            stmt.setString(2, card.getCardHolderName());
            stmt.setString(3, card.getExpiryDate());
            stmt.setInt(4, card.getSecurityCode());
            stmt.setString(5, card.getBankName());
            stmt.setInt(6, card.getLimit());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void addDebitCard(Connection con, DebitCard card) {
        
        String query = "insert into debit_card values(?,?,?,?,?,?)";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, card.getCardNumber());
            stmt.setString(2, card.getCardHolderName());
            stmt.setString(3, card.getExpiryDate());
            stmt.setInt(4, card.getSecurityCode());
            stmt.setString(5, card.getBankName());
            stmt.setDouble(6, card.getFunds());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void addPersonalCard(Connection con, PersonalCard card) {
        
        String query = "insert into personal_card values(?,?,?)";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setString(1, card.getCardNumber());
            stmt.setString(2, card.getCardHolderName());
            if (card.getExpiryDate() != null){
                stmt.setString(3, card.getExpiryDate());
            }else{
                stmt.setString(3, null);
            }
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void addNote(Connection con, Note note){

        String query = "insert into note values(?,?,?,?,?,?,?)";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setInt(1, note.getNoteId());
            stmt.setString(2, note.getTitle());
            stmt.setString(3, note.getBody());
            if (note.getIsReminderActive() == false){
                stmt.setInt(4, 0);
            }else{
                stmt.setInt(4, 1);
            }
            stmt.setInt(5, note.getReminderMinutes());
            stmt.setInt(6, note.getReminderHours());
            stmt.setInt(7, note.getReminderDays());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void addCash(Connection con, Cash cash) {
        
        String query = "insert into cash values(?)";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            stmt.setDouble(1, cash.getCash());
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
