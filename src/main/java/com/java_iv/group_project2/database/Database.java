package com.java_iv.group_project2.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.java_iv.group_project2.model.Cash;
import com.java_iv.group_project2.model.CreditCard;
import com.java_iv.group_project2.model.DebitCard;
import com.java_iv.group_project2.model.Note;
import com.java_iv.group_project2.model.PersonalCard;

public class Database {
    public static void main(String[] args) {
        Connection conn = getConnection(Config.username, Config.password);
        // String query = "SELECT * from customers";
        // try (PreparedStatement stmt = conn.prepareStatement(query)){
        //     ResultSet rs = stmt.executeQuery();
        //     System.out.println();
        //     while(rs.next()){
        //         System.out.println("Name: "+ rs.getString("customername"));
        //     }
        //     System.out.println();
        // } catch (Exception e) {
        //     System.out.println(e.getMessage());
        // }

        // CreditCard card  = new CreditCard("Adam", "649846313435", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime(), 556, "TD", 5000);
        // AddToDB.addCreditCard(conn, card);
        // DebitCard card2 = new DebitCard("Paul", "649846313435", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime(), 556, "TD", 10000);
        // AddToDB.addDebitCard(conn, card2);
        // PersonalCard p_card = new PersonalCard("Dan", "545654838934", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime());
        // AddToDB.addPersonalCard(conn, p_card);
        // PersonalCard p_card2 = new PersonalCard("Dan", "545654456934");
        // AddToDB.addPersonalCard(conn, p_card2);

        // Note note1 = new Note();
        // AddToDB.addNote(conn, note1);
        // Note note2 = new Note();
        // note2.setReminderMinutes(1);
        // note2.setReminderHours(2);
        // note2.setReminderDays(3);
        // AddToDB.addNote(conn, note2);
        ClearDB.clearTables(conn);
        for (int i = 0; i < 2; i++) {
            CreditCard card  = new CreditCard("Adam", String.valueOf(i), new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime(), 556, "TD", 5000);
            AddToDB.addCreditCard(conn, card);
            DebitCard card2 = new DebitCard("Paul", String.valueOf(i), new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime(), 556, "TD", 10000);
            AddToDB.addDebitCard(conn, card2);
            PersonalCard p_card = new PersonalCard("Dan", String.valueOf(i), new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime());
            PersonalCard p_card2 = new PersonalCard("Dan", "545654456934");
            AddToDB.addPersonalCard(conn, p_card);
            AddToDB.addPersonalCard(conn, p_card2);
            Note note1 = new Note();
            AddToDB.addNote(conn, note1);
        }
        Cash cash = new Cash();
        cash.setCash(1000.25);
        AddToDB.addCash(conn, cash);
//        ClearDB.clearTables(conn);


    }

    private static Connection getConnection(String usr, String pwd) {
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        Connection con = null;
        try {
            con = DriverManager.getConnection(url, usr, pwd);
            System.out.println("Connected to the server");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("Connection to database - FAILED\n");
        }
        return con;

    }
}
