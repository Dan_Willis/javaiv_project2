package com.java_iv.group_project2.database;

import java.sql.*;

public class GetFromDB {
    public static int getLastNoteId(Connection con){
        String query = "select max(note_id)from note";
        try {
            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            stmt.close();
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
