package unit;
import com.java_iv.group_project2.model.Cash;

import org.junit.Test;
import junit.framework.Assert;

public class TestCash {
    @Test  
    public void testAddCash(){
        Cash money = new Cash();
        money.addCash(100);
        Assert.assertEquals(100.0, money.getCash());
    } 

    @Test  
    public void testRemoveCash(){
        Cash money = new Cash();
        Assert.assertFalse(money.removeCash(10));
        money.addCash(100);
        Assert.assertTrue(money.removeCash(50));  
        Assert.assertEquals(50.0, money.getCash());
    }

    @Test
    public void testGetCash(){
        Cash money = new Cash(500);
        Assert.assertEquals(500.0, money.getCash());
    }

}
