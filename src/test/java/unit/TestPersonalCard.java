package unit;
import org.junit.Test;
import junit.framework.Assert;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.java_iv.group_project2.model.PersonalCard;

public class TestPersonalCard {
    @Test
     public void testCreatePersonalCardNoDate() {
         PersonalCard card = new PersonalCard("Dan", "545654838934");

         Assert.assertEquals("Dan", card.getCardHolderName());
         Assert.assertEquals("545654838934", card.getCardNumber());
     }

    @Test
    public void testCreatePersonalCardWithDate() {
        PersonalCard card = new PersonalCard("Dan", "545654838934", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime());
        
        Assert.assertEquals("Dan", card.getCardHolderName());
        Assert.assertEquals("545654838934", card.getCardNumber());
        Assert.assertEquals("02/2024", card.getExpiryDate());
    }
}


