package unit;
import java.util.Date;

import com.java_iv.group_project2.model.Note;

import org.junit.Test;
import junit.framework.Assert;

public class TestNote {
    @Test
    public void testCreateNote(){
        Note note = new Note();
        Date date = new Date();
        Assert.assertEquals(0, date.compareTo(note.getCreationDate()));
        Assert.assertEquals("My new note", note.getTitle());
        Assert.assertEquals("Enter text here", note.getBody());
        Assert.assertFalse(note.getIsReminderActive());
        Assert.assertEquals(0, note.getReminderMinutes());
        Assert.assertEquals(0, note.getReminderHours());
        Assert.assertEquals(0, note.getReminderDays());
    }

    @Test
    public void modifyNote(){
        Note note = new Note();
        note.setTitle("Shopping list"); 
        note.setBody("I neeed to buy food");
        note.setReminderActive(true);
        note.setReminderMinutes(10);
        note.setReminderHours(5);
        note.setReminderDays(1);

        Assert.assertEquals("Shopping list", note.getTitle());
        Assert.assertEquals("I neeed to buy food", note.getBody());
        Assert.assertTrue(note.getIsReminderActive());
        Assert.assertEquals(10, note.getReminderMinutes());
        Assert.assertEquals(5, note.getReminderHours());
        Assert.assertEquals(1, note.getReminderDays());
    }
}
