package unit;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.java_iv.group_project2.model.DebitCard;

import org.junit.Test;
import junit.framework.Assert;

public class TestDebitCard {
    @Test
    public void testCreateDebitCard(){
        DebitCard card = new DebitCard("Paul", "649846313435", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime(), 556, "TD", 10000);
        Assert.assertEquals("Paul", card.getCardHolderName());
        Assert.assertEquals("649846313435", card.getCardNumber());
        Assert.assertEquals("02/2024", card.getExpiryDate());
        Assert.assertEquals(556, card.getSecurityCode());
        Assert.assertEquals("TD", card.getBankName());
        Assert.assertEquals(10000.0, card.getFunds());
    }

    @Test
    public void testWithdraw(){
        DebitCard card = new DebitCard("Paul", "649846313435", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime(), 556, "TD", 10000);
        try {
            Assert.assertTrue(card.withdraw(10000));
            Assert.assertFalse(card.withdraw(10001));
        } catch (Exception e) {
            Assert.fail("Operation failed: " + e.getMessage());
        }
        
    }
}
