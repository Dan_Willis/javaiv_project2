package unit;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.java_iv.group_project2.model.CreditCard;
import com.java_iv.group_project2.model.DebitCard;
import com.java_iv.group_project2.model.EWallet;
import com.java_iv.group_project2.model.Note;
import com.java_iv.group_project2.model.PersonalCard;

import org.junit.Test;
import junit.framework.Assert;

public class TestEWallet {
    @Test
    public void testAddCard(){
        EWallet wallet = new EWallet();
        
        try {
            Assert.assertTrue(wallet.addCard("Dan", "5648648468466"));
            Assert.assertTrue(wallet.addCard("Dan", "5648648498466", new GregorianCalendar(2024, Calendar.SEPTEMBER, 5).getTime()));
            Assert.assertTrue(wallet.addCard("Paul", "5468469684646", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime()));
            Assert.assertTrue(wallet.addCard("Adam", "649847313435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000));
            Assert.assertTrue(wallet.addCard("John", "649841313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 556, "TD", 5000));
        } catch (Exception e) {
            Assert.fail("Operation failed: "+  e.getMessage());
        }
        
        Assert.assertEquals(5, wallet.getSize());
    }

    @Test
    public void testAddMoreThan10Cards(){
        EWallet wallet = new EWallet();
        try{
            Assert.assertTrue(wallet.addCard("Dan", "5648648468466"));
            Assert.assertTrue(wallet.addCard("John", "564685648468466"));
            Assert.assertTrue(wallet.addCard("Alice", "56486483913466"));
            Assert.assertTrue(wallet.addCard("Laurie", "5648643178466"));
            Assert.assertTrue(wallet.addCard("Gabriel", "9718648468466"));
            Assert.assertTrue(wallet.addCard("Ben", "5648646468466"));
            Assert.assertTrue(wallet.addCard("Kessie", "564864648468466"));
            Assert.assertTrue(wallet.addCard("Marie", "566846568466"));
            Assert.assertTrue(wallet.addCard("Karine", "5648446466"));
            Assert.assertTrue(wallet.addCard("Jason", "646464846331466"));
            Assert.assertFalse(wallet.addCard("Brandon", "564864844466"));
        }catch(Exception e){
            Assert.fail("Operation failed: "+  e.getMessage());
        }
        Assert.assertEquals(10, wallet.getSize());
    }

    @Test
    public void testAddNote(){
        EWallet wallet = new EWallet();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        Assert.assertEquals(3, wallet.getSize());
        Assert.assertEquals(3, wallet.listOfNotes().size());
    }

    @Test
    public void testAddMoreThan10Notes(){
        EWallet wallet = new EWallet();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        Assert.assertEquals(-1, wallet.addNote());
        Assert.assertEquals(10, wallet.getSize());
    }

    @Test
    public void testListOfCreditCards(){
        EWallet wallet = new EWallet();
        List<CreditCard> list = new ArrayList<>();
        Note n1 = new Note();
        Note n2 = new Note();
        PersonalCard pc1 = new PersonalCard("Dan", "5648648468466");
        PersonalCard pc2 = new PersonalCard("Daniel", "5648648468466", new GregorianCalendar(2024, Calendar.SEPTEMBER, 5).getTime());
        PersonalCard pc3 = new PersonalCard("Paul", "5468464684646", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime());
        DebitCard dc1 = new DebitCard("Adam", "649846313435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000);
        DebitCard dc2 = new DebitCard("John", "649846313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 556, "TD", 5000);
        CreditCard cc1 = new CreditCard("Alice", "649846313435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000);
        CreditCard cc2 = new CreditCard("James", "649846313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 556, "TD", 5000);
        list.add(cc1);
        list.add(cc2);
        for (int i = 0; i < wallet.listOfCreditCards().size(); i++) {
            Assert.assertEquals(list.get(i), wallet.listOfCreditCards().get(i));
        }
    }

    @Test
    public void testListOfDebitCards(){
        EWallet wallet = new EWallet();
        List<DebitCard> list = new ArrayList<>();
        Note n1 = new Note();
        Note n2 = new Note();
        PersonalCard pc1 = new PersonalCard("Dan", "5648648468466");
        PersonalCard pc2 = new PersonalCard("Daniel", "5648648468466", new GregorianCalendar(2024, Calendar.SEPTEMBER, 5).getTime());
        PersonalCard pc3 = new PersonalCard("Paul", "5468464684646", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime());
        DebitCard dc1 = new DebitCard("Adam", "649846313435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000);
        DebitCard dc2 = new DebitCard("John", "649846313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 556, "TD", 5000);
        CreditCard cc1 = new CreditCard("Alice", "649846313435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000);
        CreditCard cc2 = new CreditCard("James", "649846313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 556, "TD", 5000);
        list.add(dc1);
        list.add(dc2);
        for (int i = 0; i < wallet.listOfDebitCards().size(); i++) {
            Assert.assertEquals(list.get(i), wallet.listOfCreditCards().get(i));
        }
    }

    @Test
    public void testListOfPersonalCards(){
        EWallet wallet = new EWallet();
        List<PersonalCard> list = new ArrayList<>();
        Note n1 = new Note();
        Note n2 = new Note();
        PersonalCard pc1 = new PersonalCard("Dan", "5648648468466");
        PersonalCard pc2 = new PersonalCard("Daniel", "5648648468466", new GregorianCalendar(2024, Calendar.SEPTEMBER, 5).getTime());
        PersonalCard pc3 = new PersonalCard("Paul", "5468464684646", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime());
        DebitCard dc1 = new DebitCard("Adam", "649846313435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000);
        DebitCard dc2 = new DebitCard("John", "649846313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 556, "TD", 5000);
        CreditCard cc1 = new CreditCard("Alice", "649846313435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000);
        CreditCard cc2 = new CreditCard("James", "649846313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 556, "TD", 5000);
        list.add(pc1);
        list.add(pc2);
        list.add(pc3);
        for (int i = 0; i < wallet.listOfPersonalCards().size(); i++) {
            Assert.assertEquals(list.get(i), wallet.listOfCreditCards().get(i));
        }
    }

    @Test
    public void testListOfNotes(){
        EWallet wallet = new EWallet();
        List<Note> list = new ArrayList<>();
        Note n1 = new Note();
        Note n2 = new Note();
        PersonalCard pc1 = new PersonalCard("Dan", "5648648468466");
        PersonalCard pc2 = new PersonalCard("Daniel", "5648648468466", new GregorianCalendar(2024, Calendar.SEPTEMBER, 5).getTime());
        PersonalCard pc3 = new PersonalCard("Paul", "5468464684646", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime());
        DebitCard dc1 = new DebitCard("Adam", "649846313435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000);
        DebitCard dc2 = new DebitCard("John", "649846313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 556, "TD", 5000);
        CreditCard cc1 = new CreditCard("Alice", "649846313435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000);
        CreditCard cc2 = new CreditCard("James", "649846313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 556, "TD", 5000);
        list.add(n1);
        list.add(n2);
        for (int i = 0; i < wallet.listOfNotes().size(); i++) {
            Assert.assertEquals(list.get(i), wallet.listOfNotes().get(i));
        }
    }

    @Test
    public void testRemoveCard(){
        EWallet wallet = new EWallet();
        
        try {
            wallet.addCard("Dan", "5648648468466");
            wallet.addCard("Daniel", "5645618468466", new GregorianCalendar(2024, Calendar.SEPTEMBER, 5).getTime());
            wallet.addCard("Paul", "5468464884646", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime());
            wallet.addCard("Adam", "649846383435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000);
            wallet.addCard("John", "649843313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 556, "TD", 5000);
        } catch (Exception e) {
            Assert.fail("Operation failed: "+  e.getMessage());
        }
        
        Assert.assertEquals(5, wallet.getSize());
        wallet.removeCard("5648648468466");
        Assert.assertEquals(4, wallet.getSize());
    }

    @Test 
    public void testRemoveNote(){
        EWallet wallet = new EWallet();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        Assert.assertEquals(3, wallet.getSize());
        wallet.removeNote(wallet.listOfNotes().get(wallet.listOfNotes().size() - 1).getNoteId());
        Assert.assertEquals(2, wallet.getSize());
    }

    @Test
    public void testFindNote(){
        EWallet wallet = new EWallet();
        wallet.addNote();
        wallet.addNote();
        wallet.addNote();
        Note note = wallet.findNote(wallet.listOfNotes().get(wallet.listOfNotes().size() - 1).getNoteId());
        Assert.assertEquals(wallet.listOfNotes().get(2), note);
    }

    @Test
    public void testUpdateNote(){
        EWallet wallet = new EWallet();
        wallet.addNote();
        wallet.updateNote(wallet.listOfNotes().get(wallet.listOfNotes().size() - 1).getNoteId(), "New Title", "This is the body");
        Assert.assertEquals("New Title", wallet.listOfNotes().get(0).getTitle());
        Assert.assertEquals("This is the body", wallet.listOfNotes().get(0).getBody());

        wallet.addNote();
        wallet.updateNote(wallet.listOfNotes().get(wallet.listOfNotes().size() - 1).getNoteId(), "There is no Title", "There is no body", 1, 2, 3);
        Assert.assertEquals("There is no Title", wallet.listOfNotes().get(1).getTitle());
        Assert.assertEquals("There is no body", wallet.listOfNotes().get(1).getBody());
        Assert.assertEquals(1, wallet.listOfNotes().get(1).getReminderMinutes());
        Assert.assertEquals(2, wallet.listOfNotes().get(1).getReminderHours());
        Assert.assertEquals(3, wallet.listOfNotes().get(1).getReminderDays());
    }

    @Test
    public void testGetSize(){
        EWallet wallet = new EWallet();
        
        try {
            Assert.assertTrue(wallet.addCard("Dan", "5648648468466"));
            Assert.assertTrue(wallet.addCard("Dan", "5648648418466", new GregorianCalendar(2024, Calendar.SEPTEMBER, 5).getTime()));
            Assert.assertTrue(wallet.addCard("Paul", "5467464684646", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime()));
            Assert.assertTrue(wallet.addCard("Adam", "649840313435", new GregorianCalendar(2024, Calendar.JANUARY, 12).getTime(), 556, "TD", 10000));
            Assert.assertTrue(wallet.addCard("John", "649846313435", new GregorianCalendar(2024, Calendar.MARCH, 13).getTime(), 565, "TD", 5000));
        } catch (Exception e) {
            Assert.fail("Operation failed: "+  e.getMessage());
        }
        
        wallet.addNote();
        wallet.addNote();
        Assert.assertEquals(7, wallet.getSize());
    }
}
