package unit;
import com.java_iv.group_project2.model.WalletDB;
import com.java_iv.group_project2.model.EWallet;

import org.junit.Test;
import junit.framework.Assert;

public class TestWalletDB {
    @Test
    public void testSaveEWalletToDB(){
        WalletDB db = new WalletDB();
        EWallet wallet = new EWallet();
        try {
            wallet.addCard("Dan", "465464646465");
        } catch (Exception e) {
            Assert.fail("Operation failed: "+  e.getMessage());
        }
        wallet.addNote();
        db.saveEWalletToDB(wallet);
        Assert.assertEquals(wallet.getSize(), db.loadEWalletFromDB().getSize());
    }

    @Test
    public void testLoadEWalletToDB(){
        WalletDB db = new WalletDB();
        EWallet wallet = new EWallet();
        try {
            wallet.addCard("Dan", "465464646465");
        } catch (Exception e) {
            Assert.fail("Operation failed: "+  e.getMessage());
        }
        wallet.addNote();
        db.saveEWalletToDB(wallet);
        EWallet loadedWallet = db.loadEWalletFromDB();
        Assert.assertEquals(2, loadedWallet.getSize());
    }
}
