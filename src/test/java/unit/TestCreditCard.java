package unit;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.java_iv.group_project2.model.CreditCard;
import org.junit.Test;
import junit.framework.Assert;


public class TestCreditCard {
    @Test
    public void testCreateCreditCard(){
        CreditCard card  = new CreditCard("Adam", "649846313435", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime(), 556, "TD", 5000);
        Assert.assertEquals("Adam", card.getCardHolderName());
        Assert.assertEquals("649846313435", card.getCardNumber());
        Assert.assertEquals("02/2024", card.getExpiryDate());
        Assert.assertEquals(556, card.getSecurityCode());
        Assert.assertEquals("TD", card.getBankName());
        Assert.assertEquals(5000, card.getLimit());
    }

    @Test
    public void testSpend(){
        CreditCard card  = new CreditCard("Adam", "649846313435", new GregorianCalendar(2024, Calendar.FEBRUARY, 11).getTime(), 556, "TD", 1000);
        try {
            Assert.assertTrue(card.spend(1000));
            Assert.assertFalse(card.spend(1001));
        } catch (Exception e) {
           Assert.fail("Operation failed: " + e.getMessage());
        }
        
        
        
    }
}
