package unit;
import com.java_iv.group_project2.model.DebitCardIssuer;

import org.junit.Test;
import junit.framework.Assert;

public class TestDebitCardIssuer {
    @Test
    public void testCreateDebitCardIssuer(){
        DebitCardIssuer account = new DebitCardIssuer("TD", 1000);
        Assert.assertEquals("TD", account.getBankName());
        Assert.assertEquals(1000.0, account.getBalance());
    }

    @Test
    public void testWithdraw(){
        DebitCardIssuer account = new DebitCardIssuer("TD", 1000);
        try {
            account.withdraw(500);
        } catch (Exception e) {
            Assert.fail("Operation failed: "+ e.getMessage());
        }
        Assert.assertEquals(500.0, account.getBalance());

    }
}
