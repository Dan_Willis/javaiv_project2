package unit;
import com.java_iv.group_project2.model.CreditCardIssuer;

import org.junit.Test;
import junit.framework.Assert;

public class TestCreditCardIssuer {
    @Test
    public void testCreatCreditCardIssuer(){
        CreditCardIssuer account = new CreditCardIssuer("Desjardins", 1000);
        Assert.assertEquals("Desjardins", account.getBankName());
        Assert.assertEquals(1000, account.getLimit());
    }

    @Test 
    public void testSpend(){
        CreditCardIssuer account = new CreditCardIssuer("Dejardins", 1000);
        try {
            Assert.assertFalse(account.spend(1001));
            Assert.assertTrue(account.spend(200));
        } catch (Exception e) {
            Assert.fail("Operation failed: "+  e.getMessage());
           
        }
        
        Assert.assertEquals(200.0, account.getMoneySpent());
    }
}
