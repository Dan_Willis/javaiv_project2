DROP TABLE credit_card;
DROP TABLE debit_card;
DROP TABLE personal_card;
DROP TABLE cash;
DROP TABLE note;

CREATE TABLE credit_card (
    credit_card_number VARCHAR2(16) PRIMARY KEY NOT NULL,
    card_holder_name VARCHAR2(50) NOT NULL,
    expirydate CHAR(7) NOT NULL,
    security_code NUMBER(3,0) NOT NULL,
    bank_name VARCHAR2(50) NOT NULL,
    limit   NUMBER NOT NULL
);

CREATE TABLE debit_card (
    debit_card_number VARCHAR2(16) PRIMARY KEY NOT NULL,
    card_holder_name VARCHAR2(50) NOT NULL,
    expirydate CHAR(7) NOT NULL,
    security_code NUMBER(3,0) NOT NULL,
    bank_name VARCHAR2(50) NOT NULL,
    funds   NUMBER NOT NULL
);

CREATE TABLE personal_card (
    personal_card_number VARCHAR2(16) PRIMARY KEY NOT NULL,
    card_holder_name VARCHAR2(50) NOT NULL,
    expirydate CHAR(7) NULL
);

CREATE TABLE cash (
    cash_amount NUMBER NOT NULL
);

CREATE TABLE note (
    note_id NUMBER(10)PRIMARY KEY NOT NULL,
    title VARCHAR2(50),
    body VARCHAR2(2000),
    is_reminder_active number(1,0),
    reminder_minutes number(2,0),
    reminder_hours number(2,0),
    reminder_days number(2,0)
);